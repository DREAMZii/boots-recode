package net.gommehd.lobbyshop.runnable;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import net.gommehd.gameapi.task.TaskHelper;
import net.gommehd.gameapi.task.type.RepeatingTask;
import net.gommehd.lobbyshop.LobbyShop;
import net.gommehd.lobbyshop.object.CustomPlayer;
import net.gommehd.lobbyshop.object.boots.Boots;

import java.util.UUID;

/**
 * @author Timmi
 */
@Getter
@NoArgsConstructor( access = AccessLevel.PRIVATE )
public class SneakRunnable implements Runnable {

    @Getter
    private static final SneakRunnable instance = new SneakRunnable();

    private RepeatingTask task;

    public void start() {
        this.task = TaskHelper.runAsyncRepeatingTask( LobbyShop.getInstance(), this, 0, 125 );
    }

    @Override
    public void run() {
        for ( CustomPlayer customPlayer : CustomPlayer.getOnlinePlayers() ) {
            try {
                Boots boots = customPlayer.getCurrentBoots();

                if ( boots == null || !LobbyShop.getBootCache().getBootsFor( UUID.fromString( customPlayer.getUuid() ) ).contains( boots.getId() ) ) {
                    continue;
                }

                boots.onPlayerSneak( customPlayer.getPlayer(), customPlayer.getPlayer().isSneaking() );
            } catch(Throwable t) {} // Ignored
        }
    }
}


