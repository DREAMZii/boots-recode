package net.gommehd.lobbyshop.cache;

import net.gommehd.gameapi.database.DBResult;
import net.gommehd.lobbyshop.LobbyShop;
import net.gommehd.network.protocol.util.Util;
import org.bukkit.Bukkit;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by Fabian on 18.09.2014.
 */
public class BootCache {
    private Map<UUID, Set<Integer>> bootCache = new ConcurrentHashMap<>();

    public void init() {
        bootCache.clear();
        int size = 0;

        DBResult resultSet = LobbyShop.getBootsConnection().query( "SELECT * FROM `boots_ownerships`" );
        if ( resultSet != null && resultSet.getRows().size() > 0 ) {
            for ( DBResult.DBRow row : resultSet.getRows() ) {
                String uuidString = row.getString( "uuid" );
                if ( uuidString.length() < 32 ) {
                    continue;
                }

                UUID uuid = Util.convertUUID( uuidString );
                size++;

                if ( size % 100000 == 0 ) {
                    Bukkit.getLogger().info( "LobbyShop size: " + size );
                }

                // New entry
                if ( !bootCache.containsKey( uuid ) ) {
                    bootCache.put( uuid, new HashSet<>() );
                }

                bootCache.get( uuid ).add( row.getInt( "boot_id" ) );
            }
        }
    }

    public void updateCacheFor( UUID uuid ) {
        DBResult resultSet = LobbyShop.getBootsConnection().query( "SELECT `boot_id` FROM `boots_ownerships` WHERE `uuid` = ?", uuid.toString().replaceAll( "-", "" ) );
        if ( resultSet != null && resultSet.getRows().size() > 0 ) {
            // New entry
            if ( !bootCache.containsKey( uuid ) ) {
                bootCache.put( uuid, new HashSet<>() );
            }

            bootCache.get( uuid ).clear();

            for( DBResult.DBRow row : resultSet.getRows() ) {
                bootCache.get( uuid ).add( row.getInt( "boot_id" ) );
            }
        }
    }

    public Set<Integer> getBootsFor( UUID uuid ) {
        return bootCache.get( uuid );
    }
}
