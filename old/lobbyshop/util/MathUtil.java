package net.gommehd.lobbyshop.util;

/**
 * @author Timmi
 */
public class MathUtil {

    /**
     * Adds up an integer to a specific value
     *
     * @param from Integer to add up
     * @param to   Value to add up to
     * @return the added up {@link java.lang.Integer}
     */
    public static int addTo( int from, int to ) {
        return from % to == 0 ? from : addTo( from + 1, to );
    }

    /**
     * Test if a String is numeric
     *
     * @param s String to test
     * @return Is the String numeric?
     */
    public static boolean isNumeric( String s ) {
        try {
            Integer.parseInt( s );
            return true;
        } catch ( NumberFormatException e ) {
            return false;
        }
    }

}
