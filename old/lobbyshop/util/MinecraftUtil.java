package net.gommehd.lobbyshop.util;

import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;

import java.util.List;

/**
 * @author Timmi
 */
public class MinecraftUtil {

    /**
     * Converts a Location into a pretty formatted String
     *
     * @param l Location to convert
     * @return the formatted {@link java.lang.String}
     */
    public static String stringFromLocation( Location l ) {
        String world = l.getWorld().getName();
        double x = l.getX();
        double y = l.getY();
        double z = l.getZ();
        float yaw = l.getYaw();
        float pitch = l.getPitch();

        return world + ", " + x + ", " + y + ", " + z + ", " + yaw + ", " + pitch;
    }

    /**
     * Converts a String into a usable Location
     * Never use this, if the String isn't formatted the right way
     * For the right formation see the format method on top (stringFromLocation)
     *
     * @param s String to convert
     * @return the new {@link org.bukkit.Location} instance
     */
    public static Location locationfromString( String s ) {
        if ( s.split( ", " ).length < 6 ) {
            return null;
        }

        String[] splitted = s.split( ", " );
        World world = Bukkit.getWorld( splitted[0] );
        double x;
        double y;
        double z;
        float yaw;
        float pitch;
        try {
            x = Double.parseDouble( splitted[1] );
            y = Double.parseDouble( splitted[2] );
            z = Double.parseDouble( splitted[3] );
            yaw = Float.parseFloat( splitted[4] );
            pitch = Float.parseFloat( splitted[5] );
        } catch ( NumberFormatException e ) {
            // ignored, this should never happen!
            return null;
        }

        return new Location( world, x, y, z, yaw, pitch );
    }

    /**
     * Converts an ItemStack into a pretty formatted String
     *
     * @param item ItemStack to convert
     * @return the formatted {@link java.lang.String}
     */
    public static String stringFromItemStack( ItemStack item ) {
        if ( item == null ) {
            return "AIR, 1, 0, 0, null";
        }

        String mat = item.getType().name();
        int amount = item.getAmount();
        short durability = item.getDurability();
        int color = item.getItemMeta() instanceof LeatherArmorMeta ? ( (LeatherArmorMeta) item.getItemMeta() ).getColor().asRGB() : 0;

        return mat + ", " + amount + ", " + durability + ", " + color;
    }

    /**
     * Converts an ItemStack into a pretty formatted String
     *
     * @param item ItemStack to convert
     * @return the formatted {@link java.lang.String}
     */
    public static String stringFromItemStackWithName( ItemStack item ) {
        if ( item == null || item.getType() == Material.AIR ) {
            return "AIR, 1, 0, 0, null";
        }

        String mat = item.getType().name();
        int amount = item.getAmount();
        short durability = item.getDurability();
        int color = item.getItemMeta() instanceof LeatherArmorMeta ? ( (LeatherArmorMeta) item.getItemMeta() ).getColor().asRGB() : 0;
        String name = item.getItemMeta().getDisplayName();

        return mat + ", " + amount + ", " + durability + ", " + color + ", " + name;
    }

    /**
     * Converts a String into a usable ItemStack
     * Never use this, if the String isn't formatted the right way
     * For the right formation see the format method on top (stringFromItemStack)
     *
     * @param s String to convert
     * @return the new {@link org.bukkit.inventory.ItemStack} instance
     */
    public static ItemStack itemStackFromString( String s ) {
        if ( s.split( ", " ).length < 4 ) {
            return null;
        }

        String[] splitted = s.split( ", " );
        Material mat = Material.valueOf( splitted[0] );

        int amount = 0;
        Color color = null;
        short durability = 0;
        try {
            amount = Integer.parseInt( splitted[1] );
            color = Color.fromRGB( Integer.parseInt( splitted[2] ) );
            durability = Short.parseShort( splitted[3] );
        } catch ( NumberFormatException e ) {
            // ignored, this should never happen!
        }

        ItemStack returnStack = new ItemStack( mat, amount, durability );
        if ( returnStack.getItemMeta() instanceof LeatherArmorMeta ) {
            LeatherArmorMeta meta = (LeatherArmorMeta) returnStack.getItemMeta();
            meta.setColor( color );
            returnStack.setItemMeta( meta );
        }

        return returnStack;
    }

    /**
     * Converts a String into a usable ItemStack
     * Never use this, if the String isn't formatted the right way
     * For the right formation see the format method on top (stringFromItemStack)
     *
     * @param s String to convert
     * @return the new {@link org.bukkit.inventory.ItemStack} instance
     */
    public static ItemStack itemStackFromStringWithName( String s ) {
        if ( s.split( ", " ).length < 5 ) {
            return null;
        }

        String[] splitted = s.split( ", " );
        Material mat = Material.valueOf( splitted[0] );
        String name = splitted[4];

        if ( mat == Material.AIR ) {
            return new ItemStack( mat );
        }

        int amount;
        Color color;
        short durability;
        try {
            amount = Integer.parseInt( splitted[1] );
            durability = Short.parseShort( splitted[2] );
            color = Color.fromRGB( Integer.parseInt( splitted[3] ) );
        } catch ( NumberFormatException e ) {
            // ignored, this should never happen!
            return null;
        }

        ItemStack returnStack = new ItemStack( mat, amount, durability );
        ItemMeta meta = returnStack.getItemMeta();
        meta.setDisplayName( name );
        returnStack.setItemMeta( meta );

        if ( returnStack.getItemMeta() instanceof LeatherArmorMeta ) {
            LeatherArmorMeta armorMeta = (LeatherArmorMeta) returnStack.getItemMeta();
            armorMeta.setColor( color );
            returnStack.setItemMeta( armorMeta );
        }

        return returnStack;
    }

    /**
     * Converts all possible color codes of the given string
     *
     * @param convert String to convert
     * @return the converted {@link java.lang.String}
     */
    public static String convertColorCodes( String convert ) {
        return ChatColor.translateAlternateColorCodes( '&', convert );
    }

    /**
     * Compares to Location ignoring the yaw and pitch field
     *
     * @param first  First Location to compare
     * @param second Second Location to compare
     * @return Returns true when they differ
     */
    public static boolean compareLocationWithoutHead( Location first, Location second ) {
        return first.getX() != second.getX()
                || first.getY() != second.getY()
                || first.getZ() != second.getZ();
    }

    /**
     * Compares to ItemStack ignoring some unimportant fields
     *
     * @param first  First ItemStack to compare
     * @param second Second ItemStack to compare
     * @return Do they equal each other?
     */
    public static boolean compareItemStack( ItemStack first, ItemStack second ) {
        return first != null
                && second != null
                && first.hasItemMeta()
                && second.hasItemMeta()
                && first.getType() == second.getType()
                && first.getAmount() == second.getAmount()
                && first.getItemMeta().getDisplayName().equals( second.getItemMeta().getDisplayName() );
    }

    /**
     * Changes the color of an ItemStack
     *
     * @param itemStack ItemStack to change the color from
     * @param color     Color to change to
     * @return the colored {@link org.bukkit.inventory.ItemStack}
     */
    public static ItemStack changeColor( ItemStack itemStack, Color color ) {
        try {
            LeatherArmorMeta meta = (LeatherArmorMeta) itemStack.getItemMeta();
            meta.setColor( color );
            itemStack.setItemMeta( meta );

            return itemStack;
        } catch ( Exception e ) {
            return null;
        }
    }

    /**
     * Tests if a player is in the air
     *
     * @param player player to test for
     * @return Return true when the player is NOT in the air
     */
    public static boolean isNotInAir( Player player ) {
        return player.getLocation().clone().add( 0, -1, 0 ).getBlock().getType() != Material.AIR
                || player.getLocation().clone().add( 0, -0.5, 0 ).getBlock().getType() != Material.AIR;
    }

    /**
     * Sets an item displayName + Lore
     *
     * @param original item to modify
     * @param name     to apply
     * @param lore     to apply
     * @return the modified stack
     */
    public static ItemStack setItemNameAndLore( ItemStack original, String name, List<String> lore ) {
        ItemStack item = original.clone();
        ItemMeta itemData = item.getItemMeta();
        itemData.setDisplayName( name );
        itemData.setLore( lore );
        item.setItemMeta( itemData );

        return item;
    }

}
