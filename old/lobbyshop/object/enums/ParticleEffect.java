package net.gommehd.lobbyshop.object.enums;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.events.PacketContainer;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.lang.reflect.InvocationTargetException;

public enum ParticleEffect {
    HUGE_EXPLOSION( "hugeexplosion" ),
    LARGE_EXPLODE( "largeexplode" ),
    FIREWORKS_SPARK( "fireworksSpark" ),
    BUBBLE( "bubble" ),
    SUSPEND( "suspend" ),
    DEPTH_SUSPEND( "depthSuspend" ),
    TOWN_AURA( "townaura" ),
    CRIT( "crit" ),
    MAGIC_CRIT( "magicCrit" ),
    MOB_SPELL( "mobSpell" ),
    MOB_SPELL_AMBIENT( "mobSpellAmbient" ),
    SPELL( "spell" ), INSTANT_SPELL( "instantSpell" ),
    WITCH_MAGIC( "witchMagic" ), NOTE( "note" ),
    PORTAL( "portal" ),
    ENCHANTMENT_TABLE( "enchantmenttable" ),
    EXPLODE( "explode" ),
    FLAME( "flame" ),
    LAVA( "lava" ),
    FOOTSTEP( "footstep" ),
    SPLASH( "splash" ),
    LARGE_SMOKE( "largesmoke" ),
    CLOUD( "cloud" ),
    RED_DUST( "reddust" ),
    SNOWBALL_POOF( "snowballpoof" ),
    DRIP_WATER( "dripWater" ),
    DRIP_LAVA( "dripLava" ),
    SNOW_SHOVEL( "snowshovel" ),
    SLIME( "slime" ),
    HEART( "heart" ),
    SMOKE( "smoke" ),
    ANGRY_VILLAGER( "angryVillager" ),
    HAPPY_VILLAGER( "happerVillager" ),
    ICONCRACK( "iconcrack_" ),
    TILECRACK( "tilecrack_" );

    private String particleName;

    ParticleEffect( String particleName ) {
        this.particleName = particleName;
    }

    private static PacketContainer constructPacket( String particleName, Location location, float speed, int count ) {
        PacketContainer packetContainer = ProtocolLibrary.getProtocolManager().createPacket( PacketType.Play.Server.WORLD_PARTICLES );

        packetContainer.getStrings().write( 0, particleName );
        packetContainer.getFloat().write( 0, (float) location.getX() )
                .write( 1, (float) location.getY() )
                .write( 2, (float) location.getZ() )
                .write( 3, 0F )
                .write( 4, 0F )
                .write( 5, 0F )
                .write( 6, speed );
        packetContainer.getIntegers().write( 0, count );

        return packetContainer;
    }

    public static void broadcastEffect( Player sender, String particleName, Location location, float speed, int count ) {
        PacketContainer packetContainer = constructPacket( particleName, location, speed, count );

        for ( Player player : Bukkit.getOnlinePlayers() ) {
            if ( player.canSee( sender ) ) {
                try {
                    ProtocolLibrary.getProtocolManager().sendServerPacket( player, packetContainer );
                } catch ( InvocationTargetException e ) {
                    e.printStackTrace();
                }
            }
        }
    }

    PacketContainer constructPacket( Location location, float speed, int count ) {
        PacketContainer packetContainer = ProtocolLibrary.getProtocolManager().createPacket( PacketType.Play.Server.WORLD_PARTICLES );

        packetContainer.getStrings().write( 0, this.particleName );
        packetContainer.getFloat().write( 0, (float) location.getX() )
                .write( 1, (float) location.getY() )
                .write( 2, (float) location.getZ() )
                .write( 3, 0F )
                .write( 4, 0F )
                .write( 5, 0F )
                .write( 6, speed );
        packetContainer.getIntegers().write( 0, count );

        return packetContainer;
    }

    public void broadcastEffect( Player sender, Location location, float speed, int count ) {
        PacketContainer packetContainer = constructPacket( location, speed, count );

        for ( Player player : Bukkit.getOnlinePlayers() ) {
            if ( player.canSee( sender ) ) {
                try {
                    ProtocolLibrary.getProtocolManager().sendServerPacket( player, packetContainer );
                } catch ( InvocationTargetException e ) {
                    e.printStackTrace();
                }
            }
        }
    }
}

