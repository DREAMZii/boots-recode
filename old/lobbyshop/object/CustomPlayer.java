package net.gommehd.lobbyshop.object;

import lombok.Getter;
import lombok.Setter;
import net.gommehd.gameapi.inventory.api.InventoryMenu;
import net.gommehd.gameapi.inventory.api.option.ActionOption;
import net.gommehd.gameapi.inventory.api.option.NoActionOption;
import net.gommehd.gameapi.player.AbstractGamePlayer;
import net.gommehd.lobbyshop.LobbyShop;
import net.gommehd.lobbyshop.object.boots.Boots;
import net.gommehd.lobbyshop.util.MinecraftUtil;
import net.gommehd.network.hector.helper.UserHelper;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import redis.clients.jedis.Jedis;

import java.lang.reflect.Field;

/**
 * @author Timmi
 */
public class CustomPlayer extends AbstractGamePlayer {

    //==================================================//
    //            Static Methods + Fields               //
    //==================================================//

    /**
     * Tries to find a CustomPlayer instance by a name
     * If there's none, it requests a new instance and returns it
     *
     * @param name name of the player to find/create
     * @return the found or created {@link net.gommehd.lobbyshop.object.CustomPlayer}
     */
    public static CustomPlayer getCustomPlayer( String name ) {
        AbstractGamePlayer player = getPlayerMap().get( name );
        if ( player != null ) {
            return (CustomPlayer) player;
        }

        return new CustomPlayer( name );
    }

    /**
     * Tries to find a CustomPlayer instance by a name and uuid
     * If there's none, it requests a new instance and returns it
     *
     * @param name name of the player to find/create
     * @param uuid uuid of the player
     * @return the found or created {@link net.gommehd.lobbyshop.object.CustomPlayer}
     */
    public static CustomPlayer getCustomPlayer( String name, String uuid ) {
        AbstractGamePlayer player = getPlayerMap().get( name );
        if ( player != null ) {
            return (CustomPlayer) player;
        }

        return new CustomPlayer( name, uuid );
    }

    /**
     * Returns all online (!) users
     *
     * @return ArrayList of {@link net.gommehd.lobbyshop.object.CustomPlayer}
     */
    public static Set<CustomPlayer> getOnlinePlayers() {
        Set<CustomPlayer> matches = new HashSet<>();
        Bukkit.getOnlinePlayers().forEach( player -> matches.add( getCustomPlayer( player.getName() ) ) );

        return matches;
    }

    //==================================================//
    //            Instance Methods + Fields             //
    //==================================================//

    @Getter @Setter
    private Boots currentBoots;
    @Getter @Setter
    private boolean doubleJumping;

    public CustomPlayer( String name ) {
        super( name );

        if ( isOnline() ) {
            this.uuid = getPlayer().getUniqueId().toString();
        }
    }

    public CustomPlayer( String name, String uuid ) {
        super( name, uuid );
    }

    public void openBootsInventory() {
        InventoryMenu menu = new InventoryMenu( ChatColor.GOLD + "Boots-Shop", 27, false);
        Set<Integer> earnedBoots = LobbyShop.getBootCache().getBootsFor( UUID.fromString( uuid ) );
        int counter = 0;
        for ( Boots boots : Boots.getBoots().values() ) {
            ItemStack bootsItem = Boots.getBootsStack( boots );

            if ( getPlayer().hasPermission( "lobbyshop.boots.all" ) || ( earnedBoots != null && earnedBoots.contains( boots.getId() ) ) ) {
                // Player does own the boots
                String displayName = boots.getDisplayName();
                List<String> lore = new ArrayList<>();

                lore.add( ChatColor.GREEN + "Du besitzt diese Boots " );
                if ( boots.isPremium() ) {
                    lore.add( ChatColor.GOLD + "Diese Boots sind NUR für Premiums" );
                }

                bootsItem = MinecraftUtil.setItemNameAndLore( bootsItem, displayName, lore );

                menu.addOption( new ActionOption( ( player ) -> {
                            player.getInventory().setBoots( Boots.getBootsStack( boots ) );
                            CustomPlayer customPlayer = CustomPlayer.getCustomPlayer( player.getName(), player.getUniqueId().toString() );
                            customPlayer.setCurrentBoots( boots );

                            // Set setting to keep boots when switching lobbies
                            Bukkit.getScheduler().runTaskAsynchronously( LobbyShop.getInstance(), () -> UserHelper.setSetting( player, "lobby.boots", String.valueOf( boots.getId() ) ) );

                            player.closeInventory();
                        } ).setCancelOnClick( true )
                                .setItemStack( bootsItem )
                                .setPosition( counter )
                );
            } else {
                // Player does not own the boots
                String displayName = boots.getDisplayName();
                List<String> lore = new ArrayList<>();

                lore.add( ChatColor.RED + "Du besitzt diese Boots nicht" );
                if ( boots.isPremium() ) {
                    lore.add( ChatColor.GOLD + "Diese Boots sind NUR für Premiums" );
                } else {
                    lore.add( ChatColor.RED + "Weitere Infos unter: " + ChatColor.YELLOW + "buy.gommehd.net" );
                }

                bootsItem = MinecraftUtil.setItemNameAndLore( bootsItem, displayName, lore );

                try {
                    Field f = Enchantment.class.getDeclaredField( "acceptingNew" );
                    f.setAccessible( true );
                    f.set( null, true );
                } catch ( Exception e ) {
                    e.printStackTrace();
                }

                bootsItem.addUnsafeEnchantment( EnchantmentGlow.GLOW, 10 );

                menu.addOption( new NoActionOption()
                                .setCancelOnClick( true )
                                .setItemStack( bootsItem )
                                .setPosition( counter )
                );
            }

            counter++;
        }

        menu.addOption( new ActionOption( player -> {
            player.getInventory().setBoots( new ItemStack( Material.AIR ) );
            CustomPlayer customPlayer = CustomPlayer.getCustomPlayer( player.getName(), player.getUniqueId().toString() );
            customPlayer.setCurrentBoots( null );

            Bukkit.getScheduler().runTaskAsynchronously( LobbyShop.getInstance(), () -> UserHelper.setSetting( player, "lobby.boots", "no" ) );

            player.closeInventory();
        } ).setPosition( 22 ).setItemStack( new ItemStack( Material.LEATHER_BOOTS ) ).setName( ChatColor.RESET + "" + ChatColor.DARK_RED + "" + ChatColor.BOLD + "Boots entfernen" ).setCancelOnClick( true ) );

        menu.open( this.getPlayer() );
    }

    public void saveBoots( int id ) {
        String tableName = "boots_ownerships";

        Set<Integer> earnedBoots = LobbyShop.getBootCache().getBootsFor( UUID.fromString( uuid ) );
        if ( earnedBoots != null && earnedBoots.contains( id ) ) {
            return;
        }

        LobbyShop.getBootsConnection().query( "INSERT INTO `" + tableName + "`(`uuid`, `boot_id`) VALUES(?,?);", uuid.replaceAll( "-", "" ), id );

        Jedis connection = LobbyShop.getJedis().getResource();
        connection.publish( "lobbyshop", uuid );
        LobbyShop.getJedis().returnResource( connection );
    }

    public void removeBoots( int id ) {
        String tableName = "boots_ownerships";

        Set<Integer> earnedBoots = LobbyShop.getBootCache().getBootsFor( UUID.fromString( uuid ) );
        if ( earnedBoots == null || !earnedBoots.contains( id ) ) {
            return;
        }

        LobbyShop.getBootsConnection().query( "DELETE FROM `" + tableName + "` WHERE `uuid`='" + uuid.replaceAll( "-", "" ) + "' AND `boot_id`='" + id + "';" );

        Jedis connection = LobbyShop.getJedis().getResource();
        connection.publish( "lobbyshop", uuid );
        LobbyShop.getJedis().returnResource( connection );
    }
}
