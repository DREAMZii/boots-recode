package net.gommehd.lobbyshop.object;

import org.bukkit.enchantments.Enchantment;
import org.bukkit.enchantments.EnchantmentTarget;
import org.bukkit.enchantments.EnchantmentWrapper;
import org.bukkit.inventory.ItemStack;

/**
 * @author Timmi
 */
public class EnchantmentGlow extends EnchantmentWrapper {

    public static Enchantment GLOW = new EnchantmentGlow( 120 );

    public EnchantmentGlow( int id ) {
        super( id );
    }

    static {
        EnchantmentWrapper.registerEnchantment( GLOW );
    }

    @Override
    public boolean canEnchantItem( ItemStack item ) {
        return true;
    }

    @Override
    public boolean conflictsWith( Enchantment other ) {
        return false;
    }

    @Override
    public EnchantmentTarget getItemTarget() {
        return null;
    }

    @Override
    public int getMaxLevel() {
        return 10;
    }

    @Override
    public String getName() {
        return null;
    }

    @Override
    public int getStartLevel() {
        return 1;
    }

}

