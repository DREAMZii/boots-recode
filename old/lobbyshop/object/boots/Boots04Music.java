package net.gommehd.lobbyshop.object.boots;

import net.gommehd.lobbyshop.object.enums.ParticleEffect;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;


/**
 * @author Timmi
 */
public class Boots04Music extends Boots {

    protected Boots04Music() {
        super();
        setName( "Music" );
        setDisplayName( "§aMusicBoots" );
        setId( 4 );
        setMaterial( Material.LEATHER_BOOTS );
        setColor( Color.GREEN );
        setPrice( 5 );
        setPremium( true );
        setEffect( ParticleEffect.NOTE );
        setParticleSpeed( 5F );
        setCount( 1 );
    }

    @Override
    public void onPlayerSneak( Player player, boolean sneaking ) {
        if ( sneaking ) {
            player.playSound( player.getEyeLocation(), Sound.NOTE_PIANO, 1F, 1F );
        }
    }

}
