package net.gommehd.lobbyshop.object.boots;

import net.gommehd.lobbyshop.object.enums.ParticleEffect;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.entity.Player;

/**
 * @author Timmi
 */
public class Boots01Fire extends Boots {

    protected Boots01Fire() {
        super();
        setName( "Fire" );
        setDisplayName( "§cFireBoots" );
        setId( 1 );
        setMaterial( Material.LEATHER_BOOTS );
        setColor( Color.RED );
        setPrice( 5 );
        setPremium( true );
        setEffect( ParticleEffect.FLAME );
        setParticleSpeed( 0F );
        setCount( 3 );
    }

    @Override
    public void onPlayerSneak( Player player, boolean sneaking ) {
        if ( sneaking ) {
            player.setFireTicks( Integer.MAX_VALUE );
            player.setNoDamageTicks( 20 );
        } else {
            player.setFireTicks( 0 );
        }
    }

}
