package net.gommehd.lobbyshop.object.boots;

import net.gommehd.lobbyshop.object.enums.ParticleEffect;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

/**
 * @author Timmi
 */
public class Boots08Ghost extends Boots {

    protected Boots08Ghost() {
        super();
        setName( "Ghost" );
        setDisplayName( "§fGhostBoots" );
        setId( 8 );
        setMaterial( Material.LEATHER_BOOTS );
        setColor( Color.GRAY );
        setPrice( 5 );
        setPremium( true );
        setEffect( ParticleEffect.CLOUD );
        setParticleSpeed( 0F );
        setCount( 1 );
    }

    @Override
    public void onPlayerSneak( Player player, boolean sneaking ) {
        if ( sneaking ) {
            player.addPotionEffect( new PotionEffect( PotionEffectType.INVISIBILITY, Integer.MAX_VALUE, 0 ) );
        } else {
            player.removePotionEffect( PotionEffectType.INVISIBILITY );
        }
    }


}
