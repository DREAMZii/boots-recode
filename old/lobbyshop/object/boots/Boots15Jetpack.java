package net.gommehd.lobbyshop.object.boots;

import net.gommehd.lobbyshop.object.enums.ParticleEffect;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

/**
 * @author Timmi
 */
public class Boots15Jetpack extends Boots {

    protected Boots15Jetpack() {
        super();
        setName( "JetPack" );
        setDisplayName( "§7JetPackBoots" );
        setId( 15 );
        setMaterial( Material.GOLD_BOOTS );
        setColor( null );
        setPrice( 25 );
        setPremium( false );
        setEffect( ParticleEffect.CLOUD );
        setParticleSpeed( 0F );
        setCount( 1 );
    }

    @Override
    public void onPlayerSneak( Player player, boolean sneaking ) {
        if ( sneaking ) {
            player.setExp( 1F );

            Vector dir = player.getEyeLocation().getDirection().normalize();
            dir.setY( 0.25 );

            player.setVelocity( dir );
        } else {
            player.setExp( 0F );
        }
    }

}
