package net.gommehd.lobbyshop.object.boots;

import net.gommehd.lobbyshop.object.enums.ParticleEffect;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

/**
 * @author Timmi
 */
public class Boots05Ice extends Boots {

    protected Boots05Ice() {
        super();
        setName( "Ice" );
        setDisplayName( "§fIceBoots" );
        setId( 5 );
        setMaterial( Material.LEATHER_BOOTS );
        setColor( Color.WHITE );
        setPrice( 5 );
        setPremium( true );
        setEffect( ParticleEffect.CRIT );
        setParticleSpeed( 0F );
        setCount( 3 );
    }

    @Override
    public void onPlayerSneak( Player player, boolean sneaking ) {
        if ( sneaking ) {
            player.playSound( player.getEyeLocation(), Sound.DIG_STONE, 1F, 1F );
            for ( int i = 0; i < 3; i++ ) {
                ParticleEffect.broadcastEffect( player, "blockcrack_79_0", player.getEyeLocation(), 0.5F, 5 );
            }
        }
    }

}
