package net.gommehd.lobbyshop.object.boots;

import net.gommehd.lobbyshop.object.enums.ParticleEffect;
import net.gommehd.lobbyshop.util.MinecraftUtil;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

/**
 * @author Timmi
 */
public class Boots11Explosive extends Boots {

    protected Boots11Explosive() {
        super();
        setName( "Explosive" );
        setDisplayName( "§cExplosiveBoots" );
        setId( 11 );
        setMaterial( Material.IRON_BOOTS );
        setColor( null );
        setPrice( 10 );
        setPremium( false );
        setEffect( ParticleEffect.LARGE_SMOKE );
        setParticleSpeed( 0F );
        setCount( 1 );
    }

    @Override
    public void onPlayerSneak( Player player, boolean sneaking ) {
        if ( sneaking && MinecraftUtil.isNotInAir( player ) ) {
            player.setExp( (float) ( player.getExp() + 0.1 ) );

            if ( player.getExp() >= 1 ) {
                player.playSound( player.getEyeLocation(), Sound.EXPLODE, 0.5F, 1F );
                Vector dir = player.getEyeLocation().getDirection().normalize();
                dir.multiply( 8 );
                dir.setY( 1.5 );

                player.setVelocity( dir );
                player.setExp( 0F );
            }
        } else {
            player.setExp( 0F );
        }
    }

}
