package net.gommehd.lobbyshop.object.boots;

import net.gommehd.lobbyshop.LobbyShop;
import net.gommehd.lobbyshop.object.enums.ParticleEffect;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import java.util.Random;

/**
 * @author Timmi
 */
public class Boots00Rainy extends Boots {

    protected Boots00Rainy() {
        super();
        setName( "Rainy" );
        setDisplayName( "§bRainyBoots" );
        setId( 0 );
        setMaterial( Material.LEATHER_BOOTS );
        setColor( Color.AQUA );
        setPrice( 5 );
        setPremium( true );
        setEffect( ParticleEffect.SPLASH );
        setParticleSpeed( 0F );
        setCount( 5 );
    }

    @Override
    public void onPlayerSneak( Player player, boolean sneaking ) {
        if ( sneaking ) {
            Random random = LobbyShop.getRandom();
            Location loc = player.getEyeLocation().clone().add( 0, 0.5, 0 );

            for ( int i = 0; i < 5; i++ ) {
                double x = random.nextBoolean() ? random.nextDouble() : ( random.nextDouble() * -1 );
                double y = random.nextBoolean() ? random.nextDouble() : ( random.nextDouble() * -1 );
                double z = random.nextBoolean() ? random.nextDouble() : ( random.nextDouble() * -1 );

                ParticleEffect.DRIP_WATER.broadcastEffect( player, loc.clone().add( x, y, z ), 0F, 1 );
            }
        }
    }

}
