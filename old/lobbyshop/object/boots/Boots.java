package net.gommehd.lobbyshop.object.boots;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import net.gommehd.gameapi.inventory.CustomItemStack;
import net.gommehd.lobbyshop.LobbyShop;
import net.gommehd.lobbyshop.object.enums.ParticleEffect;
import net.gommehd.lobbyshop.util.MinecraftUtil;
import net.minecraft.util.com.google.common.reflect.ClassPath;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;

/**
 * @author Timmi
 */
@Getter
@NoArgsConstructor
public abstract class Boots {

    //==================================================//
    //            Static Methods + Fields               //
    //==================================================//

    @Getter private static Map<Integer, Boots> boots = new HashMap<>();

    /**
     * Initializes all existing Boots automatically
     *
     * @param classpath classpath to register
     */
    public static void init( String classpath ) {
        try {
            Field classLoader = JavaPlugin.class.getDeclaredField( "classLoader" );
            classLoader.setAccessible( true );
            ClassLoader loader = (ClassLoader) classLoader.get( LobbyShop.getInstance() );

            for ( ClassPath.ClassInfo classInfo : ClassPath.from( loader ).getTopLevelClassesRecursive( classpath ) ) {
                Class c = Class.forName( classInfo.getName() );

                if ( c.getSuperclass() != Boots.class ) {
                    continue;
                }

                Boots b = (Boots) c.newInstance();
                boots.put( b.id, b );

                LobbyShop.log( Level.INFO, "Registered Boots: " + classInfo.getName(), true );
            }
        } catch ( ClassNotFoundException | NoSuchFieldException | InstantiationException | IllegalAccessException | IOException e ) {
            e.printStackTrace();
        }
    }

    /**
     * Tests if a given player earned a specific pair of boots
     *
     * @param player player to test for
     * @param id     id of the boots
     * @return       Is the player owning these Boots?
     */
    public static boolean earnedBoots( Player player, int id ) {
        return LobbyShop.getBootCache().getBootsFor( player.getUniqueId() ) != null && LobbyShop.getBootCache().getBootsFor( player.getUniqueId() ).contains( id );
    }

    /**
     * Tries to find a Boots-instance from a given id
     *
     * @param id id of the instance to find
     * @return the found {@link net.gommehd.lobbyshop.object.boots.Boots}
     */
    public static Boots getBootsById( int id ) {
        return boots.get( id );
    }

    /**
     * Tries to find a Boots-instance from a given ItemStack
     *
     * @param item item of the instance to find
     * @return the found {@link net.gommehd.lobbyshop.object.boots.Boots}
     */
    public static Boots getBootsByItemStack( ItemStack item ) {
        for ( Boots b : boots.values() ) {
            if ( MinecraftUtil.compareItemStack( getBootsStack( b ), item ) ) {
                return b;
            }
        }

        // Nothing found
        return null;
    }

    /**
     * Tries to return the created ItemStack from a pair of boots
     *
     * @param boots Boots instance to get the ItemStack from
     * @return the created {@link net.gommehd.gameapi.inventory.CustomItemStack}
     */
    public static ItemStack getBootsStack( Boots boots ) {
        if ( boots == null ) {
            return null;
        }

        CustomItemStack item = new CustomItemStack( boots.material, 1 );
        item.changeDisplayName( boots.displayName );

        if ( boots.material == Material.LEATHER_BOOTS && boots.color != null ) {
            LeatherArmorMeta meta = (LeatherArmorMeta) item.getItemMeta();
            meta.setColor( boots.color );
            item.setItemMeta( meta );
        }

        return item;
    }

    //==================================================//
    //            Instance Methods + Fields             //
    //==================================================//

    @Setter
    private String name;
    @Setter
    private String displayName;
    @Setter
    private int id;
    @Setter
    private Material material;
    @Setter
    private Color color;
    @Setter
    private int price;
    @Setter
    private boolean premium;
    @Setter
    private ParticleEffect effect;
    @Setter
    private float particleSpeed;
    @Setter
    private int count;

    /**
     * Action if player sneaks
     */
    public abstract void onPlayerSneak( Player player, boolean sneaking );

}
