package net.gommehd.lobbyshop.object.boots;

import net.gommehd.lobbyshop.object.enums.ParticleEffect;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

/**
 * @author Timmi
 */
public class Boots06Angry extends Boots {

    protected Boots06Angry() {
        super();
        setName( "Angry" );
        setDisplayName( "§4AngryBoots" );
        setId( 6 );
        setMaterial( Material.LEATHER_BOOTS );
        setColor( Color.RED );
        setPrice( 5 );
        setPremium( true );
        setEffect( ParticleEffect.ANGRY_VILLAGER );
        setParticleSpeed( 0F );
        setCount( 1 );
    }

    @Override
    public void onPlayerSneak( Player player, boolean sneaking ) {
        if ( sneaking ) {
            Location loc = player.getEyeLocation().clone().add( 0, 0.25, 0 );

            ParticleEffect.ANGRY_VILLAGER.broadcastEffect( player, loc, 0F, 1 );
            player.playSound( player.getEyeLocation(), Sound.VILLAGER_NO, 1F, 1F );
        }
    }

}
