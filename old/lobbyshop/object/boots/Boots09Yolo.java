package net.gommehd.lobbyshop.object.boots;

import net.gommehd.lobbyshop.LobbyShop;
import net.gommehd.lobbyshop.object.enums.ParticleEffect;
import net.gommehd.lobbyshop.util.MinecraftUtil;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.entity.Player;

/**
 * @author Timmi
 */
public class Boots09Yolo extends Boots {

    private Color[] colors = new Color[]{
            Color.AQUA,
            Color.FUCHSIA,
            Color.LIME,
            Color.MAROON,
            Color.ORANGE,
            Color.PURPLE,
            Color.RED,
            Color.TEAL,
            Color.YELLOW
    };

    protected Boots09Yolo() {
        super();
        setName( "Yolo" );
        setDisplayName( "§1YoloBoots" );
        setId( 9 );
        setMaterial( Material.LEATHER_BOOTS );
        setColor( null );
        setPrice( 10 );
        setPremium( false );
        setEffect( ParticleEffect.MOB_SPELL );
        setParticleSpeed( 5F );
        setCount( 3 );
    }

    @Override
    public void onPlayerSneak( Player player, boolean sneaking ) {
        if ( sneaking ) {
            player.getInventory().setBoots( MinecraftUtil.changeColor( player.getInventory().getBoots(), colors[LobbyShop.getRandom().nextInt( colors.length )] ) );
        } else {
            player.getInventory().setBoots( getBootsStack( this ) );
        }
    }

}
