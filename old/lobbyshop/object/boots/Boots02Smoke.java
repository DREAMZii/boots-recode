package net.gommehd.lobbyshop.object.boots;

import net.gommehd.lobbyshop.object.enums.ParticleEffect;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;

/**
 * @author Timmi
 */
public class Boots02Smoke extends Boots {

    protected Boots02Smoke() {
        super();
        setName( "Smoke" );
        setDisplayName( "§7SmokeBoots" );
        setId( 2 );
        setMaterial( Material.LEATHER_BOOTS );
        setColor( Color.GRAY );
        setPrice( 5 );
        setPremium( true );
        setEffect( ParticleEffect.SMOKE );
        setParticleSpeed( 0F );
        setCount( 3 );
    }

    @Override
    public void onPlayerSneak( Player player, boolean sneaking ) {
        if ( sneaking ) {
            Location loc1 = player.getLocation();
            Location loc2 = player.getEyeLocation();

            ParticleEffect.LARGE_SMOKE.broadcastEffect( player, loc1, 0F, 3 );
            ParticleEffect.LARGE_SMOKE.broadcastEffect( player, loc2, 0F, 3 );
        }
    }

}
