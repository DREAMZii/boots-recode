package net.gommehd.lobbyshop.object.boots;

import net.gommehd.lobbyshop.object.CustomPlayer;
import net.gommehd.lobbyshop.object.enums.ParticleEffect;
import net.gommehd.lobbyshop.util.MinecraftUtil;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

/**
 * @author Timmi
 */
public class Boots14SuperJump extends Boots {

    protected Boots14SuperJump() {
        super();
        setName( "SuperJump" );
        setDisplayName( "§6SuperJumpBoots" );
        setId( 14 );
        setMaterial( Material.IRON_BOOTS );
        setColor( null );
        setPrice( 25 );
        setPremium( false );
        setEffect( ParticleEffect.RED_DUST );
        setParticleSpeed( 5F );
        setCount( 3 );
    }

    @Override
    public void onPlayerSneak( Player player, boolean sneaking ) {
        CustomPlayer customPlayer = CustomPlayer.getCustomPlayer( player.getName() );

        if ( sneaking && !MinecraftUtil.isNotInAir( player ) && customPlayer.isDoubleJumping() ) {
            Vector dir = player.getEyeLocation().getDirection().normalize();
            dir.multiply( 10 );
            dir.setY( 1.5 );

            player.setVelocity( dir );

            customPlayer.setDoubleJumping( false );
        }
    }

}
