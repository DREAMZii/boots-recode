package net.gommehd.lobbyshop.object.boots;

import net.gommehd.lobbyshop.object.enums.ParticleEffect;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

/**
 * @author Timmi
 */
public class Boots12Speed extends Boots {

    protected Boots12Speed() {
        super();
        setName( "Speed" );
        setDisplayName( "§6SpeedBoots" );
        setId( 12 );
        setMaterial( Material.IRON_BOOTS );
        setColor( null );
        setPrice( 10 );
        setPremium( false );
        setEffect( ParticleEffect.CRIT );
        setParticleSpeed( 0F );
        setCount( 3 );
    }

    @Override
    public void onPlayerSneak( Player player, boolean sneaking ) {
        if ( sneaking ) {
            player.addPotionEffect( new PotionEffect( PotionEffectType.SPEED, Integer.MAX_VALUE, 60 ) );
        } else {
            player.removePotionEffect( PotionEffectType.SPEED );
        }
    }

}
