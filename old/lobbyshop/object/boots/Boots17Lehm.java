package net.gommehd.lobbyshop.object.boots;

import net.gommehd.lobbyshop.LobbyShop;
import net.gommehd.lobbyshop.object.enums.ParticleEffect;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

import java.util.Random;

/**
 * @author Timmi
 */
public class Boots17Lehm extends Boots {

    protected Boots17Lehm() {
        super();
        setName( "Lehm" );
        setDisplayName( "§6LehmBoots" );
        setId( 17 );
        setMaterial( Material.GOLD_BOOTS );
        setColor( null );
        setPrice( 25 );
        setPremium( false );
        setEffect( ParticleEffect.PORTAL );
        setParticleSpeed( 0F );
        setCount( 3 );
    }

    @Override
    public void onPlayerSneak( Player player, boolean sneaking ) {
        if ( sneaking ) {
            player.setExp( 1F );

            Vector dir = player.getEyeLocation().getDirection().normalize();
            dir.setY( 0.25 );

            player.setVelocity( dir );

            // Displaying the LEHM
            Random random = LobbyShop.getRandom();
            Location loc = player.getLocation();
            ItemStack lehm = new ItemStack( Material.STAINED_CLAY, 1, (short) random.nextInt( 16 ) );

            for ( int i = 0; i < 3; i++ ) {
                double x = random.nextDouble() - 0.5;
                double z = random.nextDouble() - 0.5;

                // Add the Lehm
                Bukkit.getScheduler().scheduleSyncDelayedTask( LobbyShop.getInstance(), () -> {
                    Entity entity = loc.getWorld().dropItem( loc.clone().add( x, 0, z ), lehm );

                    // Remove the Lehm
                    Bukkit.getScheduler().scheduleSyncDelayedTask( LobbyShop.getInstance(), () -> entity.remove(), 10 );
                } );
            }
        } else {
            player.setExp( 0F );
        }
    }

}
