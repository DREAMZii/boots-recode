package net.gommehd.lobbyshop.object.boots;

import net.gommehd.lobbyshop.object.enums.ParticleEffect;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;

/**
 * @author Timmi
 */
public class Boots03Love extends Boots {

    protected Boots03Love() {
        super();
        setName( "Love" );
        setDisplayName( "§4LoveBoots" );
        setId( 3 );
        setMaterial( Material.LEATHER_BOOTS );
        setColor( Color.PURPLE );
        setPrice( 5 );
        setPremium( true );
        setEffect( ParticleEffect.HEART );
        setParticleSpeed( 0F );
        setCount( 1 );
    }

    @Override
    public void onPlayerSneak( Player player, boolean sneaking ) {
        if ( sneaking ) {
            Location loc = player.getEyeLocation().clone().add( 0, 0.5, 0 );

            ParticleEffect.HEART.broadcastEffect( player, loc, 0F, 1 );
        }
    }

}
