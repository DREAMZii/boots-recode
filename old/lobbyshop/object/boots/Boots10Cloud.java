package net.gommehd.lobbyshop.object.boots;

import net.gommehd.lobbyshop.object.enums.ParticleEffect;
import net.gommehd.lobbyshop.util.MinecraftUtil;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

/**
 * @author Timmi
 */
public class Boots10Cloud extends Boots {

    protected Boots10Cloud() {
        super();
        setName( "Cloud" );
        setDisplayName( "§6CloudBoots" );
        setId( 10 );
        setMaterial( Material.IRON_BOOTS );
        setColor( null );
        setPrice( 10 );
        setPremium( false );
        setEffect( ParticleEffect.CLOUD );
        setParticleSpeed( 0F );
        setCount( 1 );
    }

    @Override
    public void onPlayerSneak( Player player, boolean sneaking ) {
        if ( sneaking && MinecraftUtil.isNotInAir( player ) ) {
            player.setVelocity( new Vector( 0, 6.0, 0 ) );
        }
    }

}
