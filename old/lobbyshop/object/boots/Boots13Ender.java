package net.gommehd.lobbyshop.object.boots;

import net.gommehd.lobbyshop.LobbyShop;
import net.gommehd.lobbyshop.object.enums.ParticleEffect;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

/**
 * @author Timmi
 */
public class Boots13Ender extends Boots {

    protected Boots13Ender() {
        super();
        setName( "Ender" );
        setDisplayName( "§8EnderBoots" );
        setId( 13 );
        setMaterial( Material.IRON_BOOTS );
        setColor( null );
        setPrice( 10 );
        setPremium( false );
        setEffect( ParticleEffect.PORTAL );
        setParticleSpeed( 0F );
        setCount( 5 );
    }

    @Override
    public void onPlayerSneak( Player player, boolean sneaking ) {
        if ( sneaking ) {
            player.setExp( (float) ( player.getExp() + 0.1 ) );

            if ( player.getExp() >= 1 ) {
                player.playSound( player.getEyeLocation(), Sound.ENDERMAN_TELEPORT, 0.5F, 1F );
                Vector vector = player.getEyeLocation().getDirection().normalize().clone();
                vector.multiply( 200 );

                player.setVelocity( vector );
                Bukkit.getScheduler().runTaskLater( LobbyShop.getInstance(), () -> player.setVelocity( new Vector( 0.0, 0.0, 0.0 ) ), 3L );

                player.setExp( 0F );
            }
        } else {
            player.setExp( 0F );
        }
    }

}
