package net.gommehd.lobbyshop.object.boots;

import net.gommehd.lobbyshop.LobbyShop;
import net.gommehd.lobbyshop.object.enums.ParticleEffect;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import java.util.Random;

/**
 * @author Timmi
 */
public class Boots07Lava extends Boots {

    protected Boots07Lava() {
        super();
        setName( "Lava" );
        setDisplayName( "§6LavaBoots" );
        setId( 7 );
        setMaterial( Material.LEATHER_BOOTS );
        setColor( Color.ORANGE );
        setPrice( 5 );
        setPremium( true );
        setEffect( ParticleEffect.LAVA );
        setParticleSpeed( 0F );
        setCount( 1 );
    }

    @Override
    public void onPlayerSneak( Player player, boolean sneaking ) {
        if ( sneaking ) {
            Random random = LobbyShop.getRandom();
            Location loc = player.getEyeLocation().clone().add( 0, 0.5, 0 );

            for ( int i = 0; i < 5; i++ ) {
                double x = random.nextBoolean() ? random.nextDouble() : ( random.nextDouble() * -1 );
                double y = random.nextBoolean() ? random.nextDouble() : ( random.nextDouble() * -1 );
                double z = random.nextBoolean() ? random.nextDouble() : ( random.nextDouble() * -1 );

                ParticleEffect.DRIP_LAVA.broadcastEffect( player, loc.clone().add( x, y, z ), 0F, 1 );
            }
        }
    }

}
