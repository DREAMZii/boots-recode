package net.gommehd.lobbyshop.listener;

import net.gommehd.lobbyshop.LobbyShop;
import net.gommehd.lobbyshop.object.CustomPlayer;
import net.gommehd.lobbyshop.object.boots.Boots;
import net.gommehd.network.hector.helper.UserHelper;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

/**
 * @author Timmi
 */
public class PlayerJoinListener implements Listener {

    @EventHandler
    public void onPlayerJoin( PlayerJoinEvent event ) {
        Player player = event.getPlayer();
        CustomPlayer customPlayer = CustomPlayer.getCustomPlayer( player.getName(), player.getUniqueId().toString() );

        if ( player.hasPermission( "lobbyshop.boots.premium" ) ) {
            Bukkit.getScheduler().runTaskAsynchronously( LobbyShop.getInstance(), () -> {
                for ( int i = 0; i < 9; i++ ) {
                    customPlayer.saveBoots( i );
                }
            } );
        } else {
            Bukkit.getScheduler().runTaskAsynchronously( LobbyShop.getInstance(), () -> {
                for ( int i = 0; i < 9; i++ ) {
                    customPlayer.removeBoots( i );
                }
            } );
        }

        // Check for setting to keep boots when switching lobbies
        Bukkit.getScheduler().scheduleSyncDelayedTask( LobbyShop.getInstance(), () -> {
            UserHelper.getSetting( player, "lobby.boots", setting -> {
                if ( setting == null ) {
                    Bukkit.getScheduler().runTaskAsynchronously( LobbyShop.getInstance(), () -> UserHelper.setSetting( player, "lobby.boots", "no" ) );
                    customPlayer.setCurrentBoots( null );
                } else if ( !"no".equals( setting ) ) {
                    Integer bootId = Integer.parseInt( setting );
                    Boots boots = Boots.getBootsById( bootId );

                    if ( boots == null || !LobbyShop.getBootCache().getBootsFor( player.getUniqueId() ).contains( bootId ) ) {
                        Bukkit.getScheduler().runTaskAsynchronously( LobbyShop.getInstance(), () -> UserHelper.setSetting( player, "lobby.boots", "no" ) );
                        customPlayer.setCurrentBoots( null );
                        return;
                    }

                    player.getInventory().setBoots( Boots.getBootsStack( boots ) );
                    customPlayer.setCurrentBoots( boots );

                    player.updateInventory();
                }
            } );
        }, 20 );
    }

}
