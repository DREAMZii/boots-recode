package net.gommehd.lobbyshop.listener;

import net.gommehd.lobbyshop.object.CustomPlayer;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEntityEvent;

/**
 * @author Timmi
 */
public class PlayerInteractEntityListener implements Listener {

    @EventHandler
    public void onInteractEntity( PlayerInteractEntityEvent event ) {
        Player player = event.getPlayer();
        Entity entity = event.getRightClicked();

        if ( entity instanceof LivingEntity && entity.getType() == EntityType.VILLAGER ) {
            event.setCancelled( true );
            CustomPlayer customPlayer = CustomPlayer.getCustomPlayer( player.getName() );

            customPlayer.openBootsInventory();
        }
    }

}
