package net.gommehd.lobbyshop.listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

/**
 * @author Timmi
 */
public class EntityDamageListener implements Listener {

    @EventHandler
    public void onEntityDamage( EntityDamageEvent event ) {
        if ( event.getCause() != EntityDamageEvent.DamageCause.FALL ) {
            event.setCancelled( true );
        }
    }

}
