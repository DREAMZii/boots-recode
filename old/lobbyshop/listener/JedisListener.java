package net.gommehd.lobbyshop.listener;

import net.gommehd.gameapi.task.TaskHelper;
import net.gommehd.lobbyshop.LobbyShop;
import redis.clients.jedis.JedisPubSub;

import java.util.UUID;

/**
 * Created by Fabian on 18.09.2014.
 */
public class JedisListener extends JedisPubSub {
    @Override public void onMessage( final String channel, final String message ) {
        TaskHelper.runAsyncDelayedTask( () -> {
            if ( channel.equals( "lobbyshop" ) ) {
                UUID uuid = UUID.fromString( message );
                LobbyShop.getBootCache().updateCacheFor( uuid );
            }
        } );
    }

    @Override public void onPMessage( String s, String s2, String s3 ) {

    }

    @Override public void onSubscribe( String s, int i ) {

    }

    @Override public void onUnsubscribe( String s, int i ) {

    }

    @Override public void onPUnsubscribe( String s, int i ) {

    }

    @Override public void onPSubscribe( String s, int i ) {

    }
}
