package net.gommehd.lobbyshop.listener.boots;

import net.gommehd.lobbyshop.object.CustomPlayer;
import net.gommehd.lobbyshop.object.boots.Boots;
import net.gommehd.lobbyshop.util.MinecraftUtil;
import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

/**
 * @author Timmi
 */
public class PlayerMoveListener implements Listener {

    @EventHandler
    public void onPlayerMove( PlayerMoveEvent event ) {
        if ( !MinecraftUtil.compareLocationWithoutHead( event.getFrom(), event.getTo() ) ) {
            return;
        }

        CustomPlayer customPlayer = CustomPlayer.getCustomPlayer( event.getPlayer().getName() );
        Boots boots = customPlayer.getCurrentBoots();
        if ( boots == null || boots.getEffect() == null ) {
            return;
        }

        Location loc = event.getPlayer().getLocation().clone().add( 0, 0.25, 0 );
        boots.getEffect().broadcastEffect( event.getPlayer(), loc, boots.getParticleSpeed(), boots.getCount() );

        if ( MinecraftUtil.isNotInAir( customPlayer.getPlayer() ) && !customPlayer.isDoubleJumping() ) {
            customPlayer.setDoubleJumping( true );
        }
    }

}
