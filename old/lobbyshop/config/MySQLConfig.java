package net.gommehd.lobbyshop.config;

import net.cubespace.Yamler.Config.Config;
import net.gommehd.lobbyshop.LobbyShop;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Timmi
 */
public class MySQLConfig extends Config {

    public Map<String, MySQLItem> mySQLItem = new HashMap<>();

    public MySQLConfig() {
        File file = new File( LobbyShop.getPath(), "mysql.yml" );

        CONFIG_FILE = file;
    }

}
