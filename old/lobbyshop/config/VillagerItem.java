package net.gommehd.lobbyshop.config;

import lombok.Data;
import net.cubespace.Yamler.Config.Config;

/**
 * @author Timmi
 */
@Data
public class VillagerItem extends Config {

    private String name;
    private String location;

}
