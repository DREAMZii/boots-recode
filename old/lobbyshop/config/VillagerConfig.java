package net.gommehd.lobbyshop.config;

import net.cubespace.Yamler.Config.Config;
import net.gommehd.lobbyshop.LobbyShop;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Timmi
 */
public class VillagerConfig extends Config {

    public List<VillagerItem> villagerItem = new ArrayList<>();

    public VillagerConfig() {
        File file = new File( LobbyShop.getPath(), "villager.yml" );

        CONFIG_FILE = file;
    }

}
