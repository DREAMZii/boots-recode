package net.gommehd.lobbyshop;

import lombok.Getter;
import net.cubespace.Yamler.Config.InvalidConfigurationException;
import net.gommehd.gameapi.autoregister.Register;
import net.gommehd.gameapi.database.MySQL;
import net.gommehd.gameapi.manager.MySQLManager;
import net.gommehd.lobbyshop.cache.BootCache;
import net.gommehd.lobbyshop.command.Commandsetshop;
import net.gommehd.lobbyshop.config.MySQLConfig;
import net.gommehd.lobbyshop.config.MySQLItem;
import net.gommehd.lobbyshop.config.VillagerConfig;
import net.gommehd.lobbyshop.config.VillagerItem;
import net.gommehd.lobbyshop.listener.JedisListener;
import net.gommehd.lobbyshop.object.boots.Boots;
import net.gommehd.lobbyshop.runnable.SneakRunnable;
import net.gommehd.lobbyshop.util.MinecraftUtil;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Villager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import java.io.File;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;
import java.util.logging.Level;

/**
 * @author Timmi
 */
public class LobbyShop extends JavaPlugin {

    @Getter
    private static LobbyShop instance;
    @Getter
    private static String path = "plugins" + File.separatorChar + "LobbyShop";
    @Getter
    private static Random random;
    @Getter
    private static MySQL mcsConnection;
    @Getter
    private static MySQL bootsConnection;

    @Getter
    private static MySQLConfig mySQLConfig = new MySQLConfig();
    @Getter
    private static VillagerConfig villagerConfig = new VillagerConfig();
    @Getter
    private static BootCache bootCache = new BootCache();
    @Getter
    private static JedisPool jedis;

    @Override
    public void onEnable() {

        //==================================================//
        //            Register Plugin instances             //
        //==================================================//

        instance = this;
        random = new Random();

        //==================================================//
        //               Initialize configs                 //
        //==================================================//

        try {
            mySQLConfig.init();
            villagerConfig.init();
        } catch ( InvalidConfigurationException e ) {
            e.printStackTrace();
        }

        //==================================================//
        //                  Spawn Villager                  //
        //==================================================//

        Bukkit.getScheduler().runTaskLater( this, () -> {
            for ( VillagerItem item : villagerConfig.villagerItem ) {
                String name = item.getName();
                Location location = MinecraftUtil.locationfromString( item.getLocation() );

                final Villager villager = (Villager) location.getWorld().spawnEntity( location, EntityType.VILLAGER );

                villager.setProfession( Villager.Profession.FARMER );
                villager.setCustomNameVisible( true );
                villager.setCustomName( name );
                villager.addPotionEffect( new PotionEffect( PotionEffectType.SLOW, Integer.MAX_VALUE, 126 ) );

                Commandsetshop.villagerMap.put( villager, location );
            }
        }, 10L );

        //==================================================//
        //                   Autoregister                   //
        //==================================================//

        Register.registerCommands( this, "net.gommehd.lobbyshop.command" );
        Register.registerListenerRecursive( this, "net.gommehd.lobbyshop.listener" );

        //==================================================//
        //                  Register shops                  //
        //==================================================//

        Boots.init( "net.gommehd.lobbyshop.object.boots" );

        //==================================================//
        //                 Connect to MySQL                 //
        //==================================================//

        Map<String, MySQLItem> mySQLItems = mySQLConfig.mySQLItem;

        for ( MySQLItem mySQLItem : mySQLItems.values() ) {
            String name = mySQLItem.getName();
            String dbHost = mySQLItem.getDbHost();
            String dbName = mySQLItem.getDbName();
            String dbUser = mySQLItem.getDbUser();
            String dbPassword = mySQLItem.getDbPassword();

            if ( name != null && dbHost != null && dbName != null && dbUser != null && dbPassword != null
                    && !name.equals( "name" ) && !dbHost.equals( "dbHost" ) && !dbName.equals( "dbName" ) && !dbUser.equals( "dbUser" ) && !dbPassword.equals( "dbPassword" ) ) {
                MySQL mysql = new MySQL( name, dbHost, dbName, dbUser, dbPassword, 2 );

                mysql.connect();

                log( Level.INFO, "Connected to MySQL " + name, true );
            } else {
                log( Level.WARNING, "Could not connect to MySQL " + name, true );
                log( Level.WARNING, "MySQL seems to have an issue or this server doesn't fullfill the requirement", true );
            }
        }

        mcsConnection = MySQLManager.getInstance().getByName( "mcs" );
        bootsConnection = MySQLManager.getInstance().getByName( "boots" );

        //==================================================//
        //                   Start Runnables                //
        //==================================================//

        SneakRunnable.getInstance().start();

        Bukkit.getScheduler().runTaskTimer( LobbyShop.getInstance(), () -> {
            Iterator<LivingEntity> iterator = Commandsetshop.villagerMap.keySet().iterator();

            while ( iterator.hasNext() ) {
                LivingEntity livingEntity = iterator.next();

                if ( livingEntity == null || livingEntity.isDead() ) {
                    iterator.remove();
                    continue;
                }

                if ( livingEntity.getLocation().distanceSquared( Commandsetshop.villagerMap.get( livingEntity ) ) >= 1 ) {
                    livingEntity.teleport( Commandsetshop.villagerMap.get( livingEntity ) );
                }
            }
        }, 0L, 5L );

        //==================================================//
        //                   Init jedis                     //
        //==================================================//
        jedis = new JedisPool( new JedisPoolConfig(), "192.168.1.4", 6379, 0 );

        new Thread( () -> {
            try {
                Jedis connection = jedis.getResource();
                connection.subscribe( new JedisListener(), "lobbyshop" );
                jedis.returnResource( connection );
            } catch (Exception e) { }
        } ).start();

        //==================================================//
        //                   Get all Boots                  //
        //==================================================//
        bootCache.init();
    }

    /**
     * Logs an important note to the console
     *
     * @param level     Level of logging
     * @param msg       Message to print out
     * @param hasPrefix Decides whether the Message has the specific prefix or not
     */
    public static void log( Level level, String msg, boolean hasPrefix ) {
        if ( hasPrefix ) {
            instance.getLogger().log( level, msg );
        } else {
            instance.getServer().getLogger().log( level, msg );
        }
    }

}
