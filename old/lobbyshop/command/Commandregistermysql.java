package net.gommehd.lobbyshop.command;

import net.cubespace.Yamler.Config.InvalidConfigurationException;
import net.gommehd.gameapi.autoregister.Command;
import net.gommehd.gameapi.autoregister.RegisterCommand;
import net.gommehd.lobbyshop.LobbyShop;
import net.gommehd.lobbyshop.config.MySQLItem;
import org.bukkit.command.CommandSender;

/**
 * @author Timmi
 */
@RegisterCommand( cmdname = "registerMySQL" )
public class Commandregistermysql extends Command {

    @Override
    public void run( CommandSender sender, String[] args ) {
        if ( args.length != 5 ) {
            return;
        }

        MySQLItem item = new MySQLItem();

        item.setName( args[0] );
        item.setDbName( args[1] );
        item.setDbHost( args[2] );
        item.setDbUser( args[3] );
        item.setDbPassword( args[4] );

        LobbyShop.getMySQLConfig().mySQLItem.put( args[0], item );
        try {
            LobbyShop.getMySQLConfig().save();
            LobbyShop.getMySQLConfig().init();
        } catch ( InvalidConfigurationException e ) {
            e.printStackTrace();
        }
    }

}
