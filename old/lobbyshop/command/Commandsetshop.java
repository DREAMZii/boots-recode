package net.gommehd.lobbyshop.command;

import net.cubespace.Yamler.Config.InvalidConfigurationException;
import net.gommehd.gameapi.autoregister.Command;
import net.gommehd.gameapi.autoregister.RegisterCommand;
import net.gommehd.lobbyshop.LobbyShop;
import net.gommehd.lobbyshop.config.VillagerItem;
import net.gommehd.lobbyshop.util.MinecraftUtil;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Timmi
 */
@RegisterCommand( cmdname = "setShop", permission = "lobbyshop.command.setshop" )
public class Commandsetshop extends Command {

    public static Map<LivingEntity, Location> villagerMap = new HashMap<>();

    @Override
    public void run( Player player, String[] args ) {
        if ( args.length != 1 ) {
            return;
        }

        String name = ChatColor.translateAlternateColorCodes( '&', args[0] );

        final Villager villager = (Villager) player.getWorld().spawnEntity( player.getLocation(), EntityType.VILLAGER );

        villager.setProfession( Villager.Profession.FARMER );
        villager.setCustomNameVisible( true );
        villager.setCustomName( name );
        villager.addPotionEffect( new PotionEffect( PotionEffectType.SLOW, Integer.MAX_VALUE, 126 ) );

        villagerMap.put( villager, player.getLocation() );

        VillagerItem item = new VillagerItem();
        item.setName( name );
        item.setLocation( MinecraftUtil.stringFromLocation( player.getLocation() ) );

        LobbyShop.getVillagerConfig().villagerItem.add( item );

        try {
            LobbyShop.getVillagerConfig().save();
            LobbyShop.getVillagerConfig().init();
        } catch ( InvalidConfigurationException e ) {
            e.printStackTrace();
        }
    }

}
