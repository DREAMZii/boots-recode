package net.gommehd.lobbyshop.command;

import net.gommehd.gameapi.autoregister.Command;
import net.gommehd.gameapi.autoregister.RegisterCommand;
import net.gommehd.lobbyshop.object.CustomPlayer;
import org.bukkit.entity.Player;

/**
 * @author Timmi
 */
@RegisterCommand( cmdname = "boots", aliases = {"schuhe"} )
public class Commandboots extends Command {

    @Override
    public void run( Player sender, String[] args ) {
        CustomPlayer customPlayer = CustomPlayer.getCustomPlayer( sender.getName() );
        customPlayer.openBootsInventory();
    }

}
