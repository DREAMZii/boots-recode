package net.gommehd.lobbyshop.command;

import net.gommehd.gameapi.autoregister.Command;
import net.gommehd.gameapi.autoregister.RegisterCommand;
import net.gommehd.lobbyshop.object.boots.Boots;
import net.gommehd.lobbyshop.util.MathUtil;
import org.bukkit.entity.Player;

/**
 * @author Timmi
 */
@RegisterCommand( cmdname = "giveBoots", permission = "lobbyshop.command.giveboots" )
public class Commandgiveboots extends Command {

    @Override
    public void run( Player player, String[] args ) {
        if ( args.length < 1 || !MathUtil.isNumeric( args[0] ) ) {
            return;
        }

        int id = Integer.parseInt( args[0] );
        Boots boots = Boots.getBootsById( id );

        if ( boots == null ) {
            return;
        }

        player.getInventory().addItem( Boots.getBootsStack( boots ) );
    }

}
