package net.gommehd.lobbyshop.command;

import net.gommehd.gameapi.autoregister.Command;
import net.gommehd.gameapi.autoregister.RegisterCommand;
import net.gommehd.lobbyshop.LobbyShop;
import org.bukkit.entity.Player;

/**
 * @author Timmi
 */
@RegisterCommand( cmdname = "reloadbootcache", permission = "lobbyshop.command.reloadcache" )
public class Commandreloadbootcache extends Command {

    @Override
    public void run( Player sender, String[] args ) {
        LobbyShop.getBootCache().init();
    }

}
