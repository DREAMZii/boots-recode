package net.gommehd.lobbyshop.command;

import net.gommehd.gameapi.autoregister.Command;
import net.gommehd.gameapi.autoregister.RegisterCommand;
import net.gommehd.lobbyshop.LobbyShop;
import net.gommehd.lobbyshop.object.CustomPlayer;
import net.minecraft.server.v1_7_R4.MinecraftServer;
import net.minecraft.util.com.mojang.authlib.GameProfile;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.UUID;

/**
 * @author Timmi
 */
@RegisterCommand( cmdname = "removeBoots", permission = "lobbyshop.command.removeboots" )
public class Commandremoveboots extends Command {

    @Override
    public void run( CommandSender sender, String[] args ) {
        if ( args.length != 2 ) {
            Bukkit.getConsoleSender().sendMessage( "Arg length" );
            return;
        }

        String name = args[0];
        final int id;
        try {
            id = Integer.parseInt( args[1] );
        } catch ( NumberFormatException e ) {
            Bukkit.getConsoleSender().sendMessage( "Parsefail" );
            // Should never happen
            return;
        }

        Bukkit.getScheduler().runTaskAsynchronously( LobbyShop.getInstance(), () -> {
            boolean online = Bukkit.getPlayerExact( name ) != null && Bukkit.getPlayerExact( name ).isOnline();

            UUID uuid;
            if ( online ) {
                uuid = Bukkit.getPlayerExact( name ).getUniqueId();
            } else {
                GameProfile gameProfile = MinecraftServer.getServer().getUserCache().getProfile( name );
                uuid = gameProfile.getId();
            }

            CustomPlayer player = CustomPlayer.getCustomPlayer( name, uuid.toString() );
            player.removeBoots( id );
        } );
    }

    @Override
    public void run( Player sender, String[] args ) {
        run( (CommandSender) sender, args );
    }

}
