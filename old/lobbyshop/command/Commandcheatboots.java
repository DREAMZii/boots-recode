package net.gommehd.lobbyshop.command;

import net.gommehd.gameapi.autoregister.Command;
import net.gommehd.gameapi.autoregister.RegisterCommand;
import net.gommehd.lobbyshop.object.boots.Boots;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * @author Timmi
 */
@RegisterCommand( cmdname = "cheatBoots", permission = "lobbyshop.command.cheatboots" )
public class Commandcheatboots extends Command {

    @Override
    public void run( CommandSender sender, String[] args ) {
        if ( args.length != 1 ) {
            return;
        }

        String name = args[0];

        for ( Boots boots : Boots.getBoots().values() ) {
            Bukkit.dispatchCommand( Bukkit.getConsoleSender(), "buyBoots " + name + " " + boots.getId() );
        }
    }

    @Override
    public void run( Player sender, String[] args ) {
        run( (CommandSender) sender, args );
    }

}
