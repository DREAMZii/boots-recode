package de.dreamzii.lobbyboots.listener;

import de.dreamzii.lobbyboots.action.BootsAction;
import de.dreamzii.lobbyboots.action.BootsActionManager;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;

public class InventoryListener implements Listener {

    private final BootsActionManager manager = BootsActionManager.instance;

    @EventHandler
    public void onInventoryClick(final InventoryClickEvent event) {
        event.setCancelled(true);

        if (!isBootsInventory(event.getClickedInventory()) || !(event.getWhoClicked() instanceof Player)) {
            return;
        }

        final Player player = (Player) event.getWhoClicked();
        final BootsAction action = manager.getById(event.getSlot());
        if (action == null) {
            return;
        }

        manager.unequipBoots(player);
        manager.equipBoots(player, action);
        player.closeInventory();
    }

    private boolean isBootsInventory(final Inventory inventory) {
        return inventory != null && inventory.getTitle().contains("Boots");
    }

}
