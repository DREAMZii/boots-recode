package de.dreamzii.lobbyboots.listener;

import de.dreamzii.lobbyboots.action.BootsAction;
import de.dreamzii.lobbyboots.action.BootsActionManager;
import org.bukkit.Location;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;

public class BootsActionListener implements Listener {

    private final BootsActionManager manager = BootsActionManager.instance;

    @EventHandler
    public void onPlayerMove(final PlayerMoveEvent event) {
        final Player player = event.getPlayer();
        if (!manager.isWearingBoots(player) || !isMoving(event.getFrom(), event.getTo())) {
            return;
        }

        final BootsAction bootsAction = manager.getBootsAction(player);
        bootsAction.onActionPerformed(BootsAction.ActionType.MOVE, player);
    }

    private boolean isMoving(final Location from, final Location to) {
        return from.distanceSquared(to) > 0;
    }

    @EventHandler
    public void onItemPickUp(final PlayerPickupItemEvent event) {
        final Item item = event.getItem();
        if (item.hasMetadata("lobbyshop.not.pickup")) {
            event.setCancelled(true);
        }
    }

}
