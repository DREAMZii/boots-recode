package de.dreamzii.lobbyboots.util;

import de.dreamzii.lobbyboots.LobbyBootsPlugin;
import de.dreamzii.lobbyboots.action.BootsAction;
import net.minecraft.util.com.google.common.reflect.ClassPath;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandExecutor;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.IOException;

public class Register {

    private static final JavaPlugin plugin = LobbyBootsPlugin.instance;

    public static void registerListenerRecursive(final String classpath) {
        try {
            final ClassLoader classLoader = plugin.getClass().getClassLoader();
            for (ClassPath.ClassInfo classInfo : ClassPath.from(classLoader).getTopLevelClassesRecursive(classpath)) {
                Object instance = Class.forName(classInfo.getName(), true, classLoader).newInstance();
                if (instance instanceof Listener) {
                    plugin.getServer().getPluginManager().registerEvents((Listener) instance, plugin);
                }
            }
        } catch(ClassNotFoundException | IllegalAccessException | InstantiationException | IOException e) {
            e.printStackTrace();
        }
    }

    public static void registerCommandsRecursive(final String classpath) {
        try {
            final ClassLoader classLoader = plugin.getClass().getClassLoader();
            for (ClassPath.ClassInfo classInfo : ClassPath.from(classLoader).getTopLevelClassesRecursive(classpath)) {
                Object instance = Class.forName(classInfo.getName(), true, classLoader).newInstance();
                final String name = instance.getClass().getSimpleName().replace("Command", "").toLowerCase();
                if (instance instanceof CommandExecutor) {
                    plugin.getServer().getPluginCommand(name).setExecutor((CommandExecutor) instance);
                }
            }
        } catch(ClassNotFoundException | IllegalAccessException | InstantiationException | IOException e) {
            e.printStackTrace();
        }
    }

    public static void registerBootsRecursive(final String classpath) {
        try {
            final ClassLoader classLoader = plugin.getClass().getClassLoader();
            for (ClassPath.ClassInfo classInfo : ClassPath.from(classLoader).getTopLevelClassesRecursive(classpath)) {
                Class clazz = Class.forName(classInfo.getName(), true, classLoader);
                Class superClass = clazz.getSuperclass();
                if (superClass != BootsAction.class) {
                    continue;
                }

                ((BootsAction) clazz.newInstance()).register();
            }
        } catch(ClassNotFoundException | IllegalAccessException | InstantiationException | IOException e) {
            e.printStackTrace();
        }
    }

}
