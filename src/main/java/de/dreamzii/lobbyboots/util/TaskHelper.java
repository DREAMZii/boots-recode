package de.dreamzii.lobbyboots.util;

import de.dreamzii.lobbyboots.task.api.DelayedTask;
import de.dreamzii.lobbyboots.task.api.RepeatingTask;

public class TaskHelper {

    public static RepeatingTask runSyncRepeatingTask(Runnable runnable, long delay, long period) {
        RepeatingTask task = new RepeatingTask(runnable, delay, period, false);
        task.start();

        return task;
    }

    public static RepeatingTask runAsyncRepeatingTask(Runnable runnable, long delay, long period) {
        RepeatingTask task = new RepeatingTask(runnable, delay, period, true);
        task.start();

        return task;
    }

    public static RepeatingTask runSyncRepeatingTask(Runnable runnable, long period) {
        return runSyncRepeatingTask(runnable, 0, period);
    }

    public static RepeatingTask runAsyncRepeatingTask(Runnable runnable, long period) {
        return runAsyncRepeatingTask(runnable, 0, period);
    }

    public static DelayedTask runAsyncDelayedTask(Runnable runnable, long delay) {
        DelayedTask task = new DelayedTask(runnable, delay, true);
        task.start();

        return task;
    }

    public static DelayedTask runSyncDelayedTask(Runnable runnable) {
        return runSyncDelayedTask(runnable, 0);
    }

    public static DelayedTask runAsyncDelayedTask(Runnable runnable) {
        return runAsyncDelayedTask(runnable, 0);
    }

    public static DelayedTask runSyncDelayedTask(Runnable runnable, long delay) {
        DelayedTask task = new DelayedTask(runnable, delay, false);
        task.start();

        return task;
    }

}
