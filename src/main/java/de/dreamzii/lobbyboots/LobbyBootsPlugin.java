package de.dreamzii.lobbyboots;

import de.dreamzii.lobbyboots.mysql.MySQLConfig;
import de.dreamzii.lobbyboots.mysql.MySQLConnection;
import de.dreamzii.lobbyboots.player.BootsPlayer;
import de.dreamzii.lobbyboots.task.ActionTask;
import de.dreamzii.lobbyboots.util.Register;
import net.cubespace.Yamler.Config.InvalidConfigurationException;
import org.bukkit.plugin.java.JavaPlugin;

public class LobbyBootsPlugin extends JavaPlugin {

    public static LobbyBootsPlugin instance;

    private final MySQLConnection connection = MySQLConnection.instance;

    @Override
    public void onEnable() {
        instance = this;

        registerListenerCommandsAndBoots();
        loadConfigs();
        startTasks();
        prepareMySQL();
    }

    @Override
    public void onDisable() {

    }

    private void registerListenerCommandsAndBoots() {
        Register.registerListenerRecursive("de.dreamzii.lobbyboots.listener");
        Register.registerCommandsRecursive("de.dreamzii.lobbyboots.command");
        Register.registerBootsRecursive("de.dreamzii.lobbyboots.action");
    }

    private void loadConfigs() {
        try {
            new MySQLConfig().init();
        } catch (InvalidConfigurationException e) {
            e.printStackTrace();
        }
    }

    private void prepareMySQL() {
        connection.connect();

        if (connection.isConnected()) {
            BootsPlayer.instance.createTablesIfNotExists();
        }
    }

    private void startTasks() {
        new ActionTask().start();
    }

}
