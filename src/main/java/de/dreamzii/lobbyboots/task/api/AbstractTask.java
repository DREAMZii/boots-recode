package de.dreamzii.lobbyboots.task.api;

import lombok.Getter;
import net.minecraft.server.v1_7_R4.MinecraftServer;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Getter
public abstract class AbstractTask implements Runnable {

    @Getter
    private final Map<Integer, AbstractTask> tasks = new HashMap<>();

    protected int id;
    protected Runnable runnable;
    protected ExecutorService executorService;
    protected boolean cancelled;

    private boolean async;

    public AbstractTask(final Runnable runnable, final boolean async) {
        this.runnable = runnable;
        this.id = incrementAndGet();
        this.async = async;
        this.executorService = Executors.newCachedThreadPool();
        this.cancelled = false;
        tasks.put(this.id, this);
    }

    private int incrementAndGet() {
        int possibleId = 0;
        while (tasks.containsKey(possibleId)) {
            possibleId++;
        }

        return possibleId;
    }

    @Override
    public void run() {
        if (cancelled) {
            return;
        }

        if (async) {
            runnable.run();
        } else {
            MinecraftServer.getServer().processQueue.add(runnable);
        }
    }

    public void cancel() {
        cancelled = true;
        tasks.remove(this.id);
    }

    public abstract void start();

    public abstract void stop();

}
