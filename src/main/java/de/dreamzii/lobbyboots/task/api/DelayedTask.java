package de.dreamzii.lobbyboots.task.api;

/**
 * @author Timmi
 */
public class DelayedTask extends AbstractTask {

    protected long delay;

    public DelayedTask(final Runnable runnable, final long delay, final boolean async) {
        super(runnable, async);
        this.delay = delay;
    }

    @Override
    public void start() {
        final DelayedTask current = this;
        this.executorService.execute(() -> {
            try {
                Thread.sleep(delay);
            } catch(InterruptedException e) {
                e.printStackTrace();
            }

            current.run();
        });
    }

    @Override
    public void stop() {
        throw new RuntimeException("This Task can not be stopped [#" + this.id + "]");
    }

}
