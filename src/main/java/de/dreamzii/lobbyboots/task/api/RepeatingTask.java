package de.dreamzii.lobbyboots.task.api;

import lombok.Getter;

/**
 * @author Timmi
 */
public class RepeatingTask extends AbstractTask {

    protected long delay;
    protected long period;

    @Getter
    private boolean running;

    public RepeatingTask(final Runnable runnable, final long delay, final long period, final boolean async) {
        super(runnable, async);
        this.delay = delay;
        this.period = period;
        this.running = false;
    }

    @Override
    public void start() {
        if (running) {
            throw new RuntimeException("Task already running [#" + this.id + "]");
        }

        if (cancelled) {
            throw new RuntimeException("Task already cancelled [#" + this.id + "]");
        }

        running = true;

        final RepeatingTask current = this;
        this.executorService.execute(() -> {
            try {
                Thread.sleep(delay);
            } catch(InterruptedException e) {
                e.printStackTrace();
            }

            while(!cancelled) {
                current.run();

                try {
                    Thread.sleep(period);
                } catch(InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void stop() {
        if(!running) {
            throw new RuntimeException("Task is not running [#" + this.id + "]");
        }
        if(cancelled) {
            throw new RuntimeException("Task already cancelled [#" + this.id + "]");
        }

        running = false;
        cancel();
    }
}
