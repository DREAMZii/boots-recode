package de.dreamzii.lobbyboots.task;

import de.dreamzii.lobbyboots.action.BootsAction;
import de.dreamzii.lobbyboots.action.BootsActionManager;
import de.dreamzii.lobbyboots.action.RainyBootsAction;
import de.dreamzii.lobbyboots.util.TaskHelper;
import lombok.Getter;
import org.bukkit.util.Vector;

public class ActionTask implements Runnable {

    @Getter
    private static int tick = 0;
    private final BootsActionManager manager = BootsActionManager.instance;

    public void start() {
        TaskHelper.runAsyncRepeatingTask(this, 0, 1);
    }

    @Override
    public void run() {
        manager.getBootsActionList().forEach(action -> {
            if (action.shouldPerform()) {
                action.performAll(BootsAction.ActionType.SNEAK);
            }
        });

        tick();
    }

    private void tick() {
        if (tick < 1000) {
            tick++;
        } else {
            tick = 0;
        }
    }

}
