package de.dreamzii.lobbyboots.boots;

import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;

public class BootsItemStack extends ItemStack {

    private final Boots boots;

    public BootsItemStack(final Boots boots) {
        super(boots.getMaterial(), 1);
        this.boots = boots;
        rename();
        dye();
    }

    private void rename() {
        final ItemMeta itemMeta = getItemMeta();
        itemMeta.setDisplayName(boots.getChatColor() + boots.getName());
        setItemMeta(itemMeta);
    }

    private void dye() {
        dye(boots.getColor());
    }

    // TODO: Overall ItemStack-Clone
    public void dye(final Color color) {
        if (boots.getMaterial() != Material.LEATHER_BOOTS || color == null) {
            return;
        }

        final LeatherArmorMeta itemMeta = (LeatherArmorMeta) getItemMeta();
        itemMeta.setColor(color);
        setItemMeta(itemMeta);
    }

}
