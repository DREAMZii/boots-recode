package de.dreamzii.lobbyboots.boots;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Material;

@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class Boots {

    private int id;
    private String name;
    private Material material;
    private Color color;
    private ChatColor chatColor;
    private int price;

    public static class Builder {
        private int id;
        private String name;
        private Material material = Material.LEATHER_BOOTS;
        private Color color = Color.GRAY;
        private ChatColor chatColor = ChatColor.GRAY;
        private int price = 10;

        public Builder(final int id, final String name) {
            this.id = id;
            this.name = name;
        }

        public Builder withMaterial(final Material material) {
            this.material = material;
            return this;
        }

        public Builder withColor(final Color color) {
            this.color = color;
            return this;
        }

        public Builder withChatColor(final ChatColor chatColor) {
            this.chatColor = chatColor;
            return this;
        }

        public Builder withPrice(final int price) {
            this.price = price;
            return this;
        }

        public Boots build() {
            return new Boots(id, name, material, color, chatColor, price);
        }
    }

}
