package de.dreamzii.lobbyboots.boots;

import de.dreamzii.lobbyboots.action.BootsActionManager;
import org.bukkit.craftbukkit.v1_7_R4.inventory.CraftInventoryCustom;

public class BootsInventory extends CraftInventoryCustom {

    private final BootsActionManager manager = BootsActionManager.instance;

    public BootsInventory() {
        super(null, 18, "§6BootsInventory");
        fillInventory();
    }

    private void fillInventory() {
        manager.getSortedBootsActions().forEach(action -> addItem(new BootsItemStack(action.getBoots())));
    }

}
