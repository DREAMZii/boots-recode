package de.dreamzii.lobbyboots.mysql;

import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;

import java.sql.ResultSet;
import java.util.*;

@NoArgsConstructor
public class QueryBuilder {

    private final MySQLConnection connection = MySQLConnection.instance;

    private String tableName;
    private List<String> selectList = new ArrayList<String>();
    private Map<String, Object> insertMap = new HashMap<String, Object>();
    private Map<String, Object> deleteMap = new HashMap<String, Object>();
    private Map<String, Object> selectMap = new HashMap<String, Object>();

    public QueryBuilder table(final String tableName) {
        this.tableName = tableName;
        return this;
    }

    public QueryBuilder select(String field) {
        selectList.add(field);
        return this;
    }

    public QueryBuilder append(String field, Object object) {
        insertMap.put(field, object);
        return this;
    }

    public ResultSet execute(QueryType type) {
        if (!connection.isConnected()) {
            return null;
        }

        return connection.query(buildQuery(type), buildObjects(type));
    }

    private String buildQuery(@NonNull QueryType type) {
        switch (type) {
            case DELETE:
                return buildDelete();
            case INSERT:
                return buildInsert();
            case SELECT:
                return buildSelect();
        }

        return null;
    }

    private Object[] buildObjects(@NonNull QueryType type) {
        switch (type) {
            case DELETE:
                return deleteMap.values().toArray();
            case INSERT:
                return insertMap.values().toArray();
            case SELECT:
                return selectMap.values().toArray();
        }

        return null;
    }

    private String buildDelete() {
        StringBuilder where = new StringBuilder(" WHERE `");

        if (deleteMap.size() < 1) {
            return "";
        }

        for (Map.Entry<String, Object> entry : deleteMap.entrySet()) {
            where.append(entry.getKey()).append("` = ? AND `");
        }

        where.setLength(where.length() - 6);

        return "DELETE FROM `" + tableName + "`" + where.toString() + ";";
    }

    private String buildInsert() {
        StringBuilder columns = new StringBuilder();
        StringBuilder values = new StringBuilder();

        if (insertMap.size() < 1) {
            return "";
        }

        for (Map.Entry<String, Object> entry : insertMap.entrySet()) {
            columns.append(entry.getKey()).append("`, `");
            values.append("?, ");
        }

        columns.setLength(columns.length() - 3);
        values.setLength(values.length() - 2);

        return "INSERT INTO `" + tableName + "` (`" + columns.toString() + ") VALUES (" + values.toString() + ");";
    }

    private String buildSelect() {
        StringBuilder where = new StringBuilder(" WHERE `");
        StringBuilder select = new StringBuilder();

        if (selectList.size() < 1) {
            select.append("*");
        } else {
            select.append("`");

            for (String field : selectList) {
                select.append(field).append("`, ");
            }
            select.setLength(select.length() - 2);
        }

        for (Map.Entry<String, Object> entry : selectMap.entrySet()) {
            where.append(entry.getKey()).append("` = ? AND `");
        }

        where.setLength(where.length() - 6);

        return "SELECT " + select.toString() + " FROM `" + tableName + "`" + where.toString() + ";";
    }

    public enum QueryType {
        DELETE,
        INSERT,
        SELECT
    }

}
