package de.dreamzii.lobbyboots.mysql;

import lombok.Data;
import net.cubespace.Yamler.Config.Config;

/**
 * @author Timmi
 */
@Data
public class MySQLItem extends Config {

    private String name = "name";
    private String dbName = "dbName";
    private String dbHost = "dbHost";
    private String dbUser = "dbUser";
    private String dbPassword = "dbPassword";

}
