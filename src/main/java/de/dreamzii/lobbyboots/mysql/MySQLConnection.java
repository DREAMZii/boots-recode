package de.dreamzii.lobbyboots.mysql;

import java.sql.*;

public class MySQLConnection {

    // TODO: protect this one
    public static final MySQLConnection instance = new MySQLConnection("localhost", "lobbyboots", "root", "");

    private Connection connection;
    private String host, dbName, dbUser, dbPassword;
    private boolean autoreconnect;

    public MySQLConnection(final String host, final String dbName, final String dbUser, final String dbPassword) {
        this.host = host;
        this.dbName = dbName;
        this.dbUser = dbUser;
        this.dbPassword = dbPassword;
    }

    public void connect() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        try {
            connection = DriverManager.getConnection("jdbc:mysql://"
                    + host
                    + ":3306/"
                    + dbName
                    + (autoreconnect ? "?autoreconnect = false" : "")
                    , dbUser
                    , dbPassword);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void close() {
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ResultSet query(String query, Object... objects) {
        if (!isConnected()) {
            return null;
        }

        try {
            PreparedStatement statement = connection.prepareStatement(query);
            for (int i = 0; i < objects.length; i++) {
                Object currentObject = objects[i];
                fillStatement(currentObject, statement, i);
            }

            statement.execute();
            return statement.getResultSet();
        } catch(SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void update(String query, Object... objects) {
        if (!isConnected()) {
            return;
        }

        try {
            PreparedStatement statement = connection.prepareStatement(query);
            for (int i = 0; i < objects.length; i++) {
                Object currentObject = objects[i];
                fillStatement(currentObject, statement, i);
            }

            statement.executeUpdate();
            statement.close();
        } catch(SQLException e) {
            e.printStackTrace();
        }
    }

    private void fillStatement(final Object currentObject, final PreparedStatement statement, final int index) throws SQLException {
        if(currentObject instanceof String) {
            statement.setString(index + 1, (String) currentObject);
        } else if(currentObject instanceof Integer) {
            statement.setInt(index + 1, (Integer) currentObject);
        } else if(currentObject instanceof Long) {
            statement.setLong(index + 1, (Long) currentObject);
        } else if(currentObject instanceof Double) {
            statement.setDouble(index + 1, (Double) currentObject);
        } else if(currentObject instanceof Short) {
            statement.setShort(index + 1, (Short) currentObject);
        } else if(currentObject instanceof Float) {
            statement.setFloat(index + 1, (Float) currentObject);
        } else if(currentObject instanceof Boolean) {
            statement.setBoolean(index + 1, (Boolean) currentObject);
        }
    }

    public boolean isConnected() {
        try {
            return !connection.isClosed();
        } catch (SQLException e) {
            return false;
        }
    }

}
