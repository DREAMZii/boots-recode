package de.dreamzii.lobbyboots.mysql;

import net.cubespace.Yamler.Config.Config;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Timmi
 */
public class MySQLConfig extends Config {

    public Map<String, MySQLItem> mySQLItem = new HashMap<>();

    public MySQLConfig() {
        CONFIG_FILE = new File( "plugins" + File.separatorChar + "LobbyBoots", "mysql.yml" );
    }

}
