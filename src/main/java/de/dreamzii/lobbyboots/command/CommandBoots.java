package de.dreamzii.lobbyboots.command;

import de.dreamzii.lobbyboots.boots.BootsInventory;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandBoots implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            final Player playerSender = (Player) sender;
            playerSender.openInventory(new BootsInventory());
            playerSender.updateInventory();
        }

        return false;
    }

}
