package de.dreamzii.lobbyboots.player;

import de.dreamzii.lobbyboots.boots.Boots;
import de.dreamzii.lobbyboots.mysql.QueryBuilder;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class BootsPlayerService {

    public static BootsPlayerService instance = new BootsPlayerService();

    public void saveBootsPlayer(final BootsPlayerDto playerDto, final Boots bootsDto) {
        new QueryBuilder()
                .table("boots_" + bootsDto.getId())
                .append("name", playerDto.getName())
                .append("uuid", playerDto.getUuid().toString())
                .execute(QueryBuilder.QueryType.INSERT);
    }

}
