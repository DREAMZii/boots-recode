package de.dreamzii.lobbyboots.player;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class BootsPlayerDto {

    private String name;
    private UUID uuid;

    public static class Builder {
        private String name;
        private UUID uuid;

        public Builder(final String name, final UUID uuid) {
            this.name = name;
            this.uuid = uuid;
        }

        public BootsPlayerDto build() {
            return new BootsPlayerDto(name, uuid);
        }
    }

}
