package de.dreamzii.lobbyboots.player;

import de.dreamzii.lobbyboots.action.BootsActionManager;
import de.dreamzii.lobbyboots.mysql.MySQLConnection;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class BootsPlayer {

    public static BootsPlayer instance = new BootsPlayer();

    private final MySQLConnection connection = MySQLConnection.instance;

    public void createTablesIfNotExists() {
        if (!connection.isConnected()) {
            return;
        }

        // TODO: Better structure
        BootsActionManager.instance.getBootsActionList().forEach(action ->
            connection.query("CREATE TABLE IF NOT EXISTS boots_" +
                    action.getBoots().getId() +
                    "(id int NOT NULL AUTO_INCREMENT," +
                    "name varchar(16)," +
                    "uuid varchar(36)," +
                    "PRIMARY KEY(id)" +
                    ");")
        );
    }

}
