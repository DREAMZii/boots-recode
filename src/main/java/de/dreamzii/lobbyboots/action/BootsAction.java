package de.dreamzii.lobbyboots.action;

import de.dreamzii.lobbyboots.boots.Boots;
import de.dreamzii.lobbyboots.task.ActionTask;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.HashSet;
import java.util.Set;

public abstract class BootsAction {

    private final BootsActionManager manager = BootsActionManager.instance;

    protected final Set<Player> performingPlayers = new HashSet<>();

    public final void register() {
        manager.register(this);
        Bukkit.getLogger().info("Registered: " + getBoots().getName());
    }

    public final void addPlayer(final Player player) {
        performingPlayers.add(player);
    }

    public final void removePlayer(final Player player) {
        performingPlayers.remove(player);
    }

    public final boolean shouldPerform() {
        return performingPlayers.size() > 0;
    }

    public final Set<Player> getPerformingPlayers() {
        return performingPlayers;
    }

    public final void onActionPerformed(final ActionType actionType, final Player player) {
        if (actionType == ActionType.SNEAK) {
            onSneak(player);
        } else if (actionType == ActionType.MOVE) {
            onMove(player);
        }
    }

    public final void performAll(final ActionType actionType) {
        performingPlayers.forEach(player -> {
            if (player.isSneaking()) {
                onActionPerformed(actionType, player);
            }
        });
    }

    protected final int getTick() {
        return ActionTask.getTick();
    }

    protected abstract void onSneak(final Player player);

    protected abstract void onMove(final Player player);

    public abstract Boots getBoots();

    public static enum ActionType {
        SNEAK, MOVE
    }

}
