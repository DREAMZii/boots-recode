package de.dreamzii.lobbyboots.action;

import de.dreamzii.lobbyboots.boots.Boots;
import de.dreamzii.lobbyboots.util.ParticleEffect;
import de.dreamzii.lobbyboots.util.TaskHelper;
import lombok.Getter;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class SpeedBootsAction extends BootsAction {

    @Getter
    private final Boots boots = new Boots.Builder(12, "SpeedBoots")
            .withMaterial(Material.IRON_BOOTS)
            .withChatColor(ChatColor.GOLD)
            .withPrice(10)
            .build();

    final PotionEffect speedEffect = new PotionEffect(PotionEffectType.SPEED, 10, 60);

    @Override
    public void onSneak(final Player player) {
        TaskHelper.runSyncDelayedTask(() -> player.addPotionEffect(speedEffect));
    }

    @Override
    public void onMove(final Player player) {
        Location location = player.getLocation().clone().add(0, 0.25, 0);
        ParticleEffect.CRIT.broadcastEffect(player, location, 0F, 3);
    }

}
