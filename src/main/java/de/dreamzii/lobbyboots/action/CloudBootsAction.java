package de.dreamzii.lobbyboots.action;

import de.dreamzii.lobbyboots.boots.Boots;
import de.dreamzii.lobbyboots.util.ParticleEffect;
import lombok.Getter;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

public class CloudBootsAction extends BootsAction {

    @Getter
    private final Boots boots = new Boots.Builder(10, "CloudBoots")
            .withMaterial(Material.IRON_BOOTS)
            .withChatColor(ChatColor.GOLD)
            .withPrice(10)
            .build();

    final Vector velocityVector = new Vector(0, 6.0, 0);

    @Override
    public void onSneak(final Player player) {
        if (isNotInAir(player)) {
            player.setVelocity(velocityVector);
        }
    }

    private boolean isNotInAir(final Player player) {
        final Location playerLocation = player.getLocation();
        return playerLocation.clone().add(0, -1, 0).getBlock().getType() != Material.AIR
                || playerLocation.clone().add(0, -0.5, 0).getBlock().getType() != Material.AIR;
    }

    @Override
    public void onMove(final Player player) {
        Location location = player.getLocation().clone().add(0, 0.25, 0);
        ParticleEffect.CLOUD.broadcastEffect(player, location, 0F, 1);
    }

}
