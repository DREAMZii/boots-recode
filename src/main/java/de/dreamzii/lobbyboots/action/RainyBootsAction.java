package de.dreamzii.lobbyboots.action;

import de.dreamzii.lobbyboots.boots.Boots;
import de.dreamzii.lobbyboots.util.ParticleEffect;
import lombok.Getter;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import java.util.Random;

public class RainyBootsAction extends BootsAction {

    @Getter
    private final Boots boots = new Boots.Builder(0, "RainyBoots")
            .withMaterial(Material.LEATHER_BOOTS)
            .withColor(Color.AQUA)
            .withChatColor(ChatColor.AQUA)
            .withPrice(10)
            .build();

    private final Random random = new Random();

    final int CLOUD_TICK = 10;
    final int WATER_TICK = 25;

    @Override
    public void onSneak(final Player player) {
        final int tick = getTick();
        final Location location = player.getEyeLocation().clone().add(0, 0.5, 0);
        if (tick % CLOUD_TICK == 0) {
            spawnCloudParticle(player, location);
        }
        if (tick % WATER_TICK == 0) {
            spawnWaterParticle(player, location);
        }
    }

    private Vector getRandomVector() {
        double xAddition = random.nextBoolean() ? random.nextDouble() : (random.nextDouble() * -1);
        double zAddition = random.nextBoolean() ? random.nextDouble() : (random.nextDouble() * -1);
        return new Vector(xAddition, 0, zAddition);
    }

    private void spawnCloudParticle(final Player player, final Location location) {
        ParticleEffect.CLOUD.broadcastEffect(player, location.clone().add(getRandomVector()), 0F, 1);
    }

    private void spawnWaterParticle(final Player player, final Location location) {
        ParticleEffect.DRIP_WATER.broadcastEffect(player, location.clone().add(getRandomVector()), 0F, 1);
    }

    @Override
    public void onMove(final Player player) {
        Location location = player.getLocation().clone().add(0, 0.25, 0);
        ParticleEffect.SPLASH.broadcastEffect(player, location, 0F, 5);
    }

}
