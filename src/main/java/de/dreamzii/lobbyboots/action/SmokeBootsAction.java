package de.dreamzii.lobbyboots.action;

import de.dreamzii.lobbyboots.boots.Boots;
import de.dreamzii.lobbyboots.util.ParticleEffect;
import lombok.Getter;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;

public class SmokeBootsAction extends BootsAction {

    @Getter
    private final Boots boots = new Boots.Builder(2, "SmokeBoots")
            .withMaterial(Material.LEATHER_BOOTS)
            .withColor(Color.GRAY)
            .withChatColor(ChatColor.GRAY)
            .withPrice(10)
            .build();

    final int SMOKE_TICK = 25;

    @Override
    public void onSneak(final Player player) {
        if (getTick() % SMOKE_TICK == 0) {
            spawnSmokeParticle(player);
        }
    }

    private void spawnSmokeParticle(final Player player) {
        final Location playerLocation = player.getLocation();
        final Location playerEyeLocation = player.getEyeLocation();

        ParticleEffect.LARGE_SMOKE.broadcastEffect(player, playerLocation, 0F, 3);
        ParticleEffect.LARGE_SMOKE.broadcastEffect(player, playerEyeLocation, 0F, 3);
    }

    @Override
    public void onMove(final Player player) {
        Location location = player.getLocation().clone().add(0, 0.25, 0);
        ParticleEffect.SMOKE.broadcastEffect(player, location, 0F, 3);
    }

}
