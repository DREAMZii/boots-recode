package de.dreamzii.lobbyboots.action;

import de.dreamzii.lobbyboots.boots.Boots;
import de.dreamzii.lobbyboots.util.ParticleEffect;
import lombok.Getter;
import org.bukkit.*;
import org.bukkit.entity.Player;

public class IceBootsAction extends BootsAction {

    @Getter
    private final Boots boots = new Boots.Builder(5, "IceBoots")
            .withMaterial(Material.LEATHER_BOOTS)
            .withColor(Color.WHITE)
            .withChatColor(ChatColor.WHITE)
            .withPrice(10)
            .build();

    final int SOUND_TICK = 75;
    final int ICE_TICK = 25;

    @Override
    public void onSneak(final Player player) {
        if (getTick() % SOUND_TICK == 0) {
            player.playSound(player.getEyeLocation(), Sound.DIG_STONE, 1F, 1F);
        }
        if (getTick() % ICE_TICK == 0) {
            ParticleEffect.broadcastEffect(player, "blockcrack_79_0", player.getEyeLocation(), 0.5F, 5);
        }
    }

    @Override
    public void onMove(final Player player) {
        Location location = player.getLocation().clone().add(0, 0.25, 0);
        ParticleEffect.CRIT.broadcastEffect(player, location, 0F, 3);
    }

}
