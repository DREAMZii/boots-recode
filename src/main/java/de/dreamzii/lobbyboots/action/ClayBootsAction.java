package de.dreamzii.lobbyboots.action;

import de.dreamzii.lobbyboots.LobbyBootsPlugin;
import de.dreamzii.lobbyboots.boots.Boots;
import de.dreamzii.lobbyboots.util.TaskHelper;
import lombok.Getter;
import net.minecraft.server.v1_7_R4.EntityItem;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_7_R4.CraftWorld;
import org.bukkit.craftbukkit.v1_7_R4.inventory.CraftItemStack;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.util.Vector;

import java.util.Random;

public class ClayBootsAction extends BootsAction {

    @Getter
    private final Boots boots = new Boots.Builder(17, "LehmBoots")
            .withMaterial(Material.GOLD_BOOTS)
            .withChatColor(ChatColor.GOLD)
            .withPrice(10)
            .build();

    private final Random random = new Random();

    @Override
    public void onSneak(final Player player) {
        setFlightVelocity(player);
    }

    private void setFlightVelocity(final Player player) {
        final Vector directionVector = player.getEyeLocation().getDirection().normalize();
        directionVector.setY(0.25);
        player.setVelocity(directionVector);
    }

    @Override
    public void onMove(final Player player) {
        spawnRandomClay(player);
    }

    private void spawnRandomClay(final Player player) {
        final Location playerLocation = player.getLocation();
        final ItemStack clay = new ItemStack(Material.STAINED_CLAY, 1, (short) random.nextInt( 16 ));
        final double x = random.nextDouble() - 0.5;
        final double z = random.nextDouble() - 0.5;

        TaskHelper.runSyncDelayedTask(() -> {
            final Entity clayEntity = dropNotStackableAndPickableEntityItem(playerLocation.clone().add(x, 0, z), clay);
            TaskHelper.runSyncDelayedTask(clayEntity::remove, 500);
        } );
    }

    private Entity dropNotStackableAndPickableEntityItem(final Location location, final ItemStack itemStack) {
        final CraftWorld world = ((CraftWorld) location.getWorld());
        final EntityItem e = new EntityItem(world.getHandle(), location.getX(), location.getY(), location.getZ(), CraftItemStack.asNMSCopy(itemStack)) {
            @Override
            public boolean a(EntityItem entityitem) {
                return false;
            }
        };
        ((CraftWorld) location.getWorld()).getHandle().addEntity(e);

        final Entity bukkitEntity = e.getBukkitEntity();
        bukkitEntity.setMetadata("lobbyshop.not.pickup", new FixedMetadataValue(LobbyBootsPlugin.instance, null));
        return bukkitEntity;
    }

}
