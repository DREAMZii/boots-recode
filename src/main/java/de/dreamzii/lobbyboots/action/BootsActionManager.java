package de.dreamzii.lobbyboots.action;

import de.dreamzii.lobbyboots.boots.Boots;
import de.dreamzii.lobbyboots.boots.BootsItemStack;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.bukkit.entity.Player;

import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class BootsActionManager {

    public static final BootsActionManager instance = new BootsActionManager();

    @Getter
    private final List<BootsAction> bootsActionList = new ArrayList<>();

    public void register(final BootsAction action) {
        bootsActionList.add(action);
    }

    public BootsAction getById(final int id) {
        if (id >= bootsActionList.size()) {
            return null;
        }

        return getSortedBootsActions().get(id);
    }

    public List<BootsAction> getSortedBootsActions() {
        bootsActionList.sort((action1, action2) -> {
            if (action1.getBoots().getId() > action2.getBoots().getId()) {
                return 1;
            } else {
                return -1;
            }
        });

        return bootsActionList;
    }

    public void equipBoots(final Player player, final BootsAction bootsAction) {
        bootsAction.addPlayer(player);
        player.getInventory().setBoots(new BootsItemStack(bootsAction.getBoots()));
    }

    public void unequipBoots(final Player player) {
        bootsActionList.forEach(action -> action.removePlayer(player));
    }

    public Boots getBoots(final Player player) {
        final AtomicReference<Boots> boots = new AtomicReference<>(null);
        bootsActionList.forEach(action -> {
            if (action.performingPlayers.contains(player)) {
                boots.set(action.getBoots());
            }
        });
        return boots.get();
    }

    public BootsAction getBootsAction(final Player player) {
        final AtomicReference<BootsAction> bootsAction = new AtomicReference<>(null);
        bootsActionList.forEach(action -> {
            if (action.performingPlayers.contains(player)) {
                bootsAction.set(action);
            }
        });
        return bootsAction.get();
    }

    public boolean isWearingBoots(final Player player) {
        return getBoots(player) != null;
    }

}
