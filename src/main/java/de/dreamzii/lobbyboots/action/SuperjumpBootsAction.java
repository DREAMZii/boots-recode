package de.dreamzii.lobbyboots.action;

import de.dreamzii.lobbyboots.LobbyBootsPlugin;
import de.dreamzii.lobbyboots.boots.Boots;
import de.dreamzii.lobbyboots.util.ParticleEffect;
import lombok.Getter;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.util.Vector;

public class SuperjumpBootsAction extends BootsAction {

    @Getter
    private final Boots boots = new Boots.Builder(14, "SuperJumpBoots")
            .withMaterial(Material.IRON_BOOTS)
            .withChatColor(ChatColor.GOLD)
            .withPrice(10)
            .build();

    @Override
    public void onSneak(final Player player) {
        final boolean isDoubleJumping = player.hasMetadata("lobbyshop.doublejump") && !isNotInAir(player);
        if (isDoubleJumping) {
            performDoubleJump(player);
        }
    }

    private void performDoubleJump(final Player player) {
        final Vector dir = player.getEyeLocation().getDirection().normalize();
        dir.multiply(10);
        dir.setY(1.5);
        player.setVelocity( dir );
        player.removeMetadata("lobbyshop.doublejump", LobbyBootsPlugin.instance);
    }

    private boolean isNotInAir(final Player player) {
        final Location playerLocation = player.getLocation();
        return playerLocation.clone().add(0, -1, 0).getBlock().getType() != Material.AIR
                || playerLocation.clone().add(0, -0.5, 0).getBlock().getType() != Material.AIR;
    }

    @Override
    public void onMove(final Player player) {
        Location location = player.getLocation().clone().add(0, 0.25, 0);
        ParticleEffect.PORTAL.broadcastEffect(player, location, 0F, 5);

        // Reset DoubleJump
        if (isNotInAir(player)) {
            player.setMetadata("lobbyshop.doublejump", new FixedMetadataValue(LobbyBootsPlugin.instance, null));
        }
    }

}
