package de.dreamzii.lobbyboots.action;

import de.dreamzii.lobbyboots.boots.Boots;
import de.dreamzii.lobbyboots.util.ParticleEffect;
import lombok.Getter;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

public class ExplosiveBootsAction extends BootsAction {

    @Getter
    private final Boots boots = new Boots.Builder(11, "ExplosiveBoots")
            .withMaterial(Material.IRON_BOOTS)
            .withChatColor(ChatColor.RED)
            .withPrice(10)
            .build();

    final int EXPLODE_TICK = 100;

    @Override
    public void onSneak(final Player player) {
        if (getTick() % EXPLODE_TICK == 0 && isNotInAir(player)) {
            player.setExp((float) ( player.getExp() + 0.1 ));
            if (player.getExp() >= 1) {
                throwPlayerAndResetExp(player);
            }
        }
    }

    private void throwPlayerAndResetExp(final Player player) {
        final Vector dir = player.getEyeLocation().getDirection().normalize();
        player.playSound(player.getEyeLocation(), Sound.EXPLODE, 0.5F, 1F);
        dir.multiply(8);
        dir.setY(1.5);
        player.setVelocity(dir);
        player.setExp(0F);
    }

    private boolean isNotInAir(final Player player) {
        final Location playerLocation = player.getLocation();
        return playerLocation.clone().add(0, -1, 0).getBlock().getType() != Material.AIR
                || playerLocation.clone().add(0, -0.5, 0).getBlock().getType() != Material.AIR;
    }
    @Override
    public void onMove(final Player player) {
        Location location = player.getLocation().clone().add(0, 0.25, 0);
        ParticleEffect.LARGE_SMOKE.broadcastEffect(player, location, 0F, 1);

        // Reset Exp
        player.setExp(0F);
    }

}
