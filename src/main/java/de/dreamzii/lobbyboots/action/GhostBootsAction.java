package de.dreamzii.lobbyboots.action;

import de.dreamzii.lobbyboots.boots.Boots;
import de.dreamzii.lobbyboots.util.ParticleEffect;
import de.dreamzii.lobbyboots.util.TaskHelper;
import lombok.Getter;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class GhostBootsAction extends BootsAction {

    @Getter
    private final Boots boots = new Boots.Builder(8, "GhostBoots")
            .withMaterial(Material.LEATHER_BOOTS)
            .withColor(Color.GRAY)
            .withChatColor(ChatColor.GRAY)
            .withPrice(10)
            .build();

    final PotionEffect invisibilityEffect = new PotionEffect(PotionEffectType.INVISIBILITY, 20, 60);

    @Override
    public void onSneak(final Player player) {
        TaskHelper.runSyncDelayedTask(() -> player.addPotionEffect(invisibilityEffect));
    }

    @Override
    public void onMove(final Player player) {
        Location location = player.getLocation().clone().add(0, 0.25, 0);
        ParticleEffect.SMOKE.broadcastEffect(player, location, 0F, 1);
    }

}
