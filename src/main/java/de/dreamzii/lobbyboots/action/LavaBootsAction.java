package de.dreamzii.lobbyboots.action;

import de.dreamzii.lobbyboots.boots.Boots;
import de.dreamzii.lobbyboots.util.ParticleEffect;
import lombok.Getter;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import java.util.Random;

public class LavaBootsAction extends BootsAction {

    @Getter
    private final Boots boots = new Boots.Builder(7, "LavaBoots")
            .withMaterial(Material.LEATHER_BOOTS)
            .withColor(Color.ORANGE)
            .withChatColor(ChatColor.GOLD)
            .withPrice(10)
            .build();

    private final Random random = new Random();

    final int SMOKE_TICK = 10;
    final int LAVA_TICK = 25;
    final int EXPLODE_TICK = 500;

    @Override
    public void onSneak(final Player player) {
        final int tick = getTick();
        final Location location = player.getEyeLocation().clone().add(0, 0.5, 0);
        if (tick % SMOKE_TICK == 0) {
            spawnSmokeParticle(player, location);
        }
        if (tick % LAVA_TICK == 0) {
            spawnLavaParticle(player, location);
        }
        if (tick % EXPLODE_TICK == 0) {
            spawnExplodeParticle(player, location);
        }
    }

    private Vector getRandomVector() {
        double xAddition = random.nextBoolean() ? random.nextDouble() : (random.nextDouble() * -1);
        double zAddition = random.nextBoolean() ? random.nextDouble() : (random.nextDouble() * -1);
        return new Vector(xAddition, 0, zAddition);
    }

    private void spawnSmokeParticle(final Player player, final Location location) {
        ParticleEffect.LARGE_SMOKE.broadcastEffect(player, location.clone().add(getRandomVector()), 0F, 1);
    }

    private void spawnLavaParticle(final Player player, final Location location) {
        ParticleEffect.DRIP_LAVA.broadcastEffect(player, location.clone().add(getRandomVector()), 0F, 1);
    }

    private void spawnExplodeParticle(final Player player, final Location location) {
        ParticleEffect.LAVA.broadcastEffect(player, location.clone().add(getRandomVector()), 0F, 1);
    }

    @Override
    public void onMove(final Player player) {
        Location location = player.getLocation().clone().add(0, 0.25, 0);
        ParticleEffect.LAVA.broadcastEffect(player, location, 0F, 1);
    }

}
