package de.dreamzii.lobbyboots.action;

import de.dreamzii.lobbyboots.boots.Boots;
import de.dreamzii.lobbyboots.boots.BootsItemStack;
import de.dreamzii.lobbyboots.util.ParticleEffect;
import lombok.Getter;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import java.util.Random;

public class YoloBootsAction extends BootsAction {

    @Getter
    private final Boots boots = new Boots.Builder(9, "YoloBoots")
            .withMaterial(Material.LEATHER_BOOTS)
            .withChatColor(ChatColor.DARK_BLUE)
            .withPrice(10)
            .build();

    private final Random random = new Random();

    final int COLOR_CHANGE_TICKS = 100;
    final Color[] colors = new Color[]{
            Color.AQUA,
            Color.FUCHSIA,
            Color.LIME,
            Color.MAROON,
            Color.ORANGE,
            Color.PURPLE,
            Color.RED,
            Color.TEAL,
            Color.YELLOW
    };

    @Override
    public void onSneak(final Player player) {
        if (getTick() % COLOR_CHANGE_TICKS == 0) {
            setBootsWithRandomColor(player);
        }
    }

    private void setBootsWithRandomColor(final Player player) {
        final BootsItemStack bootsItemstack = new BootsItemStack(boots);
        final Color randomColor = colors[random.nextInt(colors.length)];
        bootsItemstack.dye(randomColor);
        player.getInventory().setBoots(bootsItemstack);
    }

    @Override
    public void onMove(final Player player) {
        Location location = player.getLocation().clone().add(0, 0.25, 0);
        ParticleEffect.MOB_SPELL.broadcastEffect(player, location, 5F, 3);
    }

}
