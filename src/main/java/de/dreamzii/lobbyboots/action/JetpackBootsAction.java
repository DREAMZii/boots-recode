package de.dreamzii.lobbyboots.action;

import de.dreamzii.lobbyboots.boots.Boots;
import de.dreamzii.lobbyboots.util.ParticleEffect;
import lombok.Getter;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

public class JetpackBootsAction extends BootsAction {

    @Getter
    private final Boots boots = new Boots.Builder(15, "JetpackBoots")
            .withMaterial(Material.GOLD_BOOTS)
            .withChatColor(ChatColor.GRAY)
            .withPrice(10)
            .build();

    @Override
    public void onSneak(final Player player) {
        final Vector directionVector = player.getEyeLocation().getDirection().normalize();
        directionVector.setY(0.25);
        player.setVelocity(directionVector);
    }

    @Override
    public void onMove(final Player player) {
        Location location = player.getLocation().clone().add(0, 0.25, 0);
        ParticleEffect.CLOUD.broadcastEffect(player, location, 0F, 1);
    }

}
