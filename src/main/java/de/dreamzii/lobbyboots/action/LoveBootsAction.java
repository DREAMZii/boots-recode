package de.dreamzii.lobbyboots.action;

import de.dreamzii.lobbyboots.boots.Boots;
import de.dreamzii.lobbyboots.util.ParticleEffect;
import lombok.Getter;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;

public class LoveBootsAction extends BootsAction {

    @Getter
    private final Boots boots = new Boots.Builder(3, "LoveBoots")
            .withMaterial(Material.LEATHER_BOOTS)
            .withColor(Color.PURPLE)
            .withChatColor(ChatColor.LIGHT_PURPLE)
            .withPrice(10)
            .build();

    final int HEART_TICK = 100;

    @Override
    public void onSneak(final Player player) {
        if (getTick() % HEART_TICK == 0) {
            spawnHeartParticle(player);
        }
    }

    private void spawnHeartParticle(final Player player) {
        Location loc = player.getEyeLocation().clone().add(0, 0.5, 0);
        ParticleEffect.HEART.broadcastEffect(player, loc, 0F, 1);
    }

    @Override
    public void onMove(final Player player) {
        Location location = player.getLocation().clone().add(0, 0.25, 0);
        ParticleEffect.HEART.broadcastEffect(player, location, 0F, 1);
    }

}
