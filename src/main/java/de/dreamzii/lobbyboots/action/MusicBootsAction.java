package de.dreamzii.lobbyboots.action;

import de.dreamzii.lobbyboots.boots.Boots;
import de.dreamzii.lobbyboots.util.ParticleEffect;
import lombok.Getter;
import org.bukkit.*;
import org.bukkit.entity.Player;

public class MusicBootsAction extends BootsAction {

    @Getter
    private final Boots boots = new Boots.Builder(4, "MusicBoots")
            .withMaterial(Material.LEATHER_BOOTS)
            .withColor(Color.GREEN)
            .withChatColor(ChatColor.GREEN)
            .withPrice(10)
            .build();

    final int SOUND_TICK = 100;

    @Override
    public void onSneak(final Player player) {
        if (getTick() % SOUND_TICK == 0) {
            player.playSound(player.getEyeLocation(), Sound.NOTE_PIANO, 1F, 1F);
        }
    }

    @Override
    public void onMove(final Player player) {
        Location location = player.getLocation().clone().add(0, 0.25, 0);
        ParticleEffect.NOTE.broadcastEffect(player, location, 5F, 1);
    }

}
