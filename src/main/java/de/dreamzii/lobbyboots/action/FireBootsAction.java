package de.dreamzii.lobbyboots.action;

import de.dreamzii.lobbyboots.boots.Boots;
import de.dreamzii.lobbyboots.util.ParticleEffect;
import lombok.Getter;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;

public class FireBootsAction extends BootsAction {

    @Getter
    private final Boots boots = new Boots.Builder(1, "FireBoots")
            .withMaterial(Material.LEATHER_BOOTS)
            .withColor(Color.RED)
            .withChatColor(ChatColor.RED)
            .withPrice(10)
            .build();

    final int FIRE_TICK = 10;

    @Override
    public void onSneak(final Player player) {
        if (getTick() % FIRE_TICK == 0) {
            player.setFireTicks(100);
        }
    }

    @Override
    public void onMove(final Player player) {
        Location location = player.getLocation().clone().add(0, 0.25, 0);
        ParticleEffect.FLAME.broadcastEffect(player, location, 0F, 3);
    }

}
