package de.dreamzii.lobbyboots.action;

import de.dreamzii.lobbyboots.boots.Boots;
import de.dreamzii.lobbyboots.util.ParticleEffect;
import lombok.Getter;
import org.bukkit.*;
import org.bukkit.entity.Player;

public class AngryBootsAction extends BootsAction {

    @Getter
    private final Boots boots = new Boots.Builder(6, "AngryBoots")
            .withMaterial(Material.LEATHER_BOOTS)
            .withColor(Color.RED)
            .withChatColor(ChatColor.RED)
            .withPrice(10)
            .build();

    final int SOUND_TICK = 100;

    @Override
    public void onSneak(final Player player) {
        if (getTick() % SOUND_TICK == 0) {
            Location loc = player.getEyeLocation().clone().add(0, 0.25, 0);
            ParticleEffect.ANGRY_VILLAGER.broadcastEffect( player, loc, 0F, 1 );
            player.playSound(player.getEyeLocation(), Sound.VILLAGER_NO, 1F, 1F);
        }
    }

    @Override
    public void onMove(final Player player) {
        Location location = player.getLocation().clone().add(0, 0.25, 0);
        ParticleEffect.ANGRY_VILLAGER.broadcastEffect(player, location, 0F, 1);
    }

}
