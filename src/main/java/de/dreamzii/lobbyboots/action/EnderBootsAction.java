package de.dreamzii.lobbyboots.action;

import de.dreamzii.lobbyboots.boots.Boots;
import de.dreamzii.lobbyboots.util.ParticleEffect;
import de.dreamzii.lobbyboots.util.TaskHelper;
import lombok.Getter;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

public class EnderBootsAction extends BootsAction {

    @Getter
    private final Boots boots = new Boots.Builder(13, "EnderBoots")
            .withMaterial(Material.IRON_BOOTS)
            .withChatColor(ChatColor.DARK_PURPLE)
            .withPrice(10)
            .build();

    final int ENDER_TICK = 100;

    @Override
    public void onSneak(final Player player) {
        if (getTick() % ENDER_TICK == 0) {
            player.setExp((float) ( player.getExp() + 0.1 ));
            if (player.getExp() >= 1) {
                teleportPlayerAndResetExp(player);
            }
        }
    }

    private void teleportPlayerAndResetExp(final Player player) {
        final Vector vector = player.getEyeLocation().getDirection().normalize().clone();
        player.playSound(player.getEyeLocation(), Sound.ENDERMAN_TELEPORT, 0.5F, 1F);
        vector.multiply(200);
        player.setVelocity(vector);
        TaskHelper.runAsyncDelayedTask(() -> player.setVelocity(new Vector(0.0, 0.0, 0.0)), 3L);
        player.setExp( 0F );
    }

    @Override
    public void onMove(final Player player) {
        Location location = player.getLocation().clone().add(0, 0.25, 0);
        ParticleEffect.PORTAL.broadcastEffect(player, location, 0F, 5);

        // Reset Exp
        player.setExp(0F);
    }

}
